/*  playergen.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_PLAYERGEN_H
#define HAVE_PLAYERGEN_H

typedef struct GwizBonusRect_ GwizBonusRect;

struct GwizBonusRect_ {
	SDL_Surface *screen;
	SDL_Surface *area;
	SDL_Surface *instructions;
	SDL_Rect bonuscursordest;
	SDL_Rect classcursordest;
	SDL_Rect bonuscursordestinit;
	SDL_Rect classcursordestinit;
	PlayerPawn pawn;
	int cursormode;
	int position;
	int maxposition;
	int availclasses;
	int bonus;
	int bonusmax;
	int min[6]; /* min, max points */
	int max[6];
	int whichclasses[8];
};

void GeneratePlayer (void);

int GenBonusPoints (void);

void NewBonusDistributor (GwizBonusRect *gwbr);

void BonusGraphicsPreload (GwizBonusRect *gwbr);

void BonusLoop (GwizBonusRect *gwbr);

void MoveBonusCursor (GwizBonusRect *gwbr, int dir);

void ChangeBonusPoints (GwizBonusRect *gwbr, int dir);

int PopulateAvailableClasses (GwizBonusRect *gwbr);

void GivePawnInitialEquipment (PlayerPawn *pawn);

void GivePawnInitialSpells (PlayerPawn *pawn);

/* useful as a debugging tool.  will go away soon. */
void EnableCheatModeForPawn (PlayerPawn *pawn);

#endif /* HAVE_PLAYERGEN_H */



