/*  encounter.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "gwiz.h"
#include "uiloop.h"
#include "text.h"
#include "maploader.h"
#include "menus.h"
#include "playerpawn.h"
#include "castle.h"
#include "playergen.h"
#include "party.h"
#include "inspect.h"
#include "encounter.h"

extern GwizApp gwiz;
GwizBattle battle;

void CheckEncounter (void)
{
    int minor = rand() % (100 - 1) + 1;
    int major = rand() % (100 - 1) + 1;
    if ((minor < 25) && (major > 65)) /* this is about a 1/3 out of 1/4 */
	Encounter(NO_NPC);
}

void Encounter (int npc)
{
    MsgBox ("An Encounter!");

    if (npc == 0)
	EncounterMonsters();
}

void EncounterMonsters(void)
{

}
