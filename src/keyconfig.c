/*  keyconfig.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <stdlib.h>
#include "gwiz.h"
#include "uiloop.h"
#include "prefsdat.h"
#include "text.h"
#include "maploader.h"
#include "menus.h"
#include "keyconfig.h"
#include "keynames.h"
#include "joystick.h"

extern GwizApp gwiz;
extern EnumStringPair keytable[];

void KeyConfig (void)
{
    char *keyjoy[] = {
	"Configure Keyboard",
	"Configure Joystick",
	NULL
    };

    if (SDL_NumJoysticks == 0)
	{
	    StartKeyConfig();
	    return;
	} else {
	    switch (NewGwizMenu (gwiz.canvas, keyjoy, -1, -1, 0))
		{
		case 0:
		    StartKeyConfig();
		    break;
		case 1:
		    JoyConfig ();
		    break;
		default:
		    return;
		}
	}
    SavePrefs();
}

void StartKeyConfig (void)
{
    char *menuitems[] = {
	"Forward",
	"Backward",
	"Turn Left",
	"Turn Right",
	"Turn Around",
	"Step Left",
	"Step Right",
	"Set \"Ok\" Button",
	"Set \"Cancel\" Button",
	"Exit Menu",
	NULL
    };    int which = 0;
	    
    while ((which = NewGwizMenu(gwiz.canvas, menuitems, -1, -1, which)) != -1)
	{
	    if (which == 9)
		break;
	    ListenKey (which); 
		}
    /* crash guard */
    SavePrefs();
}

/* FIXME:  This should also say which keys are bound to which functions */
void ListenKey (int which)
{
    SDL_Event event;
    SDL_Surface *instructions = NULL;
    SDL_Surface *bindings;
    SDL_Surface *oldcanvas;
    SDL_Rect dest;
    SDL_Rect bdest;
    
    oldcanvas = SDL_DisplayFormat(gwiz.canvas);
    
    instructions = NewTextMsg ("Press a key, ESC to cancel");
    
    dest.x = gwiz.canvas->w/2 - instructions->w/2;
    dest.y = gwiz.canvas->h - instructions->h;
    dest.h = instructions->h;
    dest.w = instructions->w;
    SDL_BlitSurface (instructions, NULL, gwiz.canvas, &dest);
    
    bindings = NewKeyBindingMsg(which);
    
    bdest.x = gwiz.canvas->w/2 - bindings->w/2;
    bdest.y = dest.y - bindings->h;
    bdest.w = bindings->w;
    bdest.h = bindings->h;
    
    SDL_BlitSurface (bindings, NULL, gwiz.canvas, &bdest);
    
    SDL_Flip (gwiz.canvas);
    dest.x = 0;
    dest.y = 0;
    dest.h = gwiz.canvas->h;
    dest.w = gwiz.canvas->w;
    
    do 
        {
	    SDL_WaitEvent (&event);
	} while (event.type != SDL_KEYDOWN);
    switch (event.type)
	{
	case SDL_KEYDOWN:
	    if ((event.key.keysym.sym == SDLK_ESCAPE) ||
		(event.key.keysym.sym == SDLK_F12) ||
		(event.key.keysym.sym == SDLK_RETURN))
		{
		    SDL_BlitSurface (oldcanvas, NULL, gwiz.canvas,
				     &dest);
		    SDL_FreeSurface (oldcanvas);
		    SDL_FreeSurface (instructions);
		    return;
		}
	    switch (which)
		{
		case 0: /* Forward */
		    gwiz.key.fwd = event.key.keysym.sym;
		    break;
		case 1: /* Backward */
		    gwiz.key.bkwd = event.key.keysym.sym;
		    break;
		case 2: /* Turn left */
		    gwiz.key.lft = event.key.keysym.sym;
		    break;
		case 3: /* Turn Right */
		    gwiz.key.rgt = event.key.keysym.sym;
		    break;
		case 4: /* Turn around */
		    gwiz.key.fta = event.key.keysym.sym;
		    break;
		case 5: /* Step left */
		    gwiz.key.slft = event.key.keysym.sym;
		    break;
		case 6: /* Step right */
		    gwiz.key.srgt = event.key.keysym.sym;
		    break;
		case 7: /* OK, or Action */
		    gwiz.key.act = event.key.keysym.sym;
		    break;
		case 8: /* Cancel */
		    gwiz.key.cancel = event.key.keysym.sym;
		    break;
		}
	case SDL_KEYUP:
	    break;
	case SDL_QUIT:
	    VerifyQuit();
	    break;
	}
    SDL_BlitSurface (oldcanvas, NULL, gwiz.canvas, &dest);
    SDL_Flip (gwiz.canvas);
    SDL_FreeSurface (oldcanvas);
    SDL_FreeSurface (instructions);
    SDL_FreeSurface (bindings);
}

SDL_Surface *NewKeyBindingMsg(which)
{
    SDL_Surface *msg;
    char cmsg[24];
    char *common = "Currently: ";
    size_t s = sizeof(char)*24;
    
    if (which == 0)
	snprintf(cmsg, s, "%s%s", common, KeyToChar(gwiz.key.fwd));
    if (which == 1)
	snprintf(cmsg, s, "%s%s", common, KeyToChar(gwiz.key.bkwd));
    if (which == 2)
	snprintf(cmsg, s, "%s%s", common, KeyToChar(gwiz.key.lft));
    if (which == 3)
	snprintf(cmsg, s, "%s%s", common, KeyToChar(gwiz.key.rgt));
    if (which == 4)
	snprintf(cmsg, s, "%s%s", common, KeyToChar(gwiz.key.fta));
    if (which == 5)
	snprintf(cmsg, s, "%s%s", common, KeyToChar(gwiz.key.slft));
    if (which == 6)
	snprintf(cmsg, s, "%s%s", common, KeyToChar(gwiz.key.srgt));
    if (which == 7)
	snprintf(cmsg, s, "%s%s", common, KeyToChar(gwiz.key.act));
    if (which == 8)
	snprintf(cmsg, s, "%s%s", common, KeyToChar(gwiz.key.cancel));
    
    msg = NewTextMsg (cmsg);
    return (msg);
}






























