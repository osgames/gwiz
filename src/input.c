#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#include <string.h>
#include "gwiz.h"
#include "text.h"
#include "input.h"
#include "keynames.h"
#include "uiloop.h"
#include "keyconfig.h"

extern GwizApp gwiz;
GwizInputConf *gic;

void RepopulateGIC (void);

void GICListen (SDL_Event *e);

void InputConfig (void)
{
    SDL_Event e;
    SDL_Rect d;

    gic = Smalloc (sizeof(gic));
 
    gic->window = NewTextBox ((gwiz.font.width * 45),
			     (gwiz.font.height * 10));

    d.x = CenterHoriz(gwiz.canvas, gic->window);
    d.y = CenterVert (gwiz.canvas, gic->window);
    d.h = gic->window->h;
    d.w = gic->window->w;

    gic->pos = 0;
    RepopulateGIC();
    SDL_BlitSurface (gic->window, NULL, gwiz.canvas, &d);
    SDL_Flip (gwiz.canvas);

    while (SDL_WaitEvent (&e) != 0)
	{
	    if (EventIsMisc(&e))
		continue;
	    if (((e.key.keysym.sym == SDLK_F11) && (e.type == SDL_KEYUP)) ||
		(e.key.keysym.sym == SDLK_ESCAPE))
		break;
	    if (e.key.keysym.sym == SDLK_F12)
		KeyConfig();
	    if (EventIsUp (&e))
		gic->pos = (gic->pos == 0 ? 0 : gic->pos - 1);
	    if (EventIsDown (&e))
		gic->pos = (gic->pos == 8 ? 8 : gic->pos + 1);
	    if (EventIsOk (&e))
		GICListen(&e);
	    RepopulateGIC();
	    SDL_BlitSurface (gic->window, NULL, gwiz.canvas, &d);
	    SDL_Flip (gwiz.canvas);
	}
    FreeSurface (&gic->window);
    Sfree (gic);
}

void RepopulateGIC (void)
{
    char *act_names[] = { "Function", /* FIXME usability */
			  "Forward",
			  "Backward",
			  "Left",
			  "Right",
			  "Step Left",
			  "Step Right",
			  "Turn Around",
			  "OK",
			  "Cancel",
			  NULL };
    char *kb_names[] = { "Keyboard",
			 KeyToChar(gwiz.key.fwd),
			 KeyToChar(gwiz.key.bkwd),
			 KeyToChar(gwiz.key.lft),
			 KeyToChar(gwiz.key.rgt),
			 KeyToChar(gwiz.key.slft),
			 KeyToChar(gwiz.key.srgt),
			 KeyToChar(gwiz.key.fta),
			 KeyToChar(gwiz.key.act),
			 KeyToChar(gwiz.key.cancel),
			 NULL };
    char col3v[9];

    SDL_Surface *column1 = NewVertTextMsg (act_names, 0);
    SDL_Surface *column2 = NewVertTextMsg (kb_names, 1);
    //    SDL_Surface *column2 = GwizRenderText ("Keyboard");
    SDL_Surface *column3 = GwizRenderText ("Joystick");
    SDL_Rect d;

    /* wipe old contents */
    d.x = 8; d.y = 8; d.h = gic->window->h - 16; d.w = gic->window->w - 16;
    SDL_FillRect (gic->window, &d, 0);

    d.x = 22; d.y = 8; d.h = column1->h; d.w = column1->w;
    SDL_BlitSurface (column1, NULL, gic->window, &d);

    d.x = gic->window->w/2 - column2->w/2; d.w = column2->w; d.h = column2->h;
    SDL_BlitSurface (column2, NULL, gic->window, &d);

    d.x = gic->window->w - 8 - column3->w;  d.y = 8; d.w = column3->w;
    SDL_BlitSurface (column3, NULL, gic->window, &d); FreeSurface (&column3);
    d.y += gwiz.font.height*5;

    snprintf (col3v, 8, "Joy %d", gwiz.joy.slft);
    column3 = GwizRenderText (col3v);
    d.w = column3->w; d.x = gic->window->w - 8 - column3->w;
    SDL_BlitSurface (column3, NULL, gic->window, &d);
    d.y += gwiz.font.height;
    FreeSurface (&column3);

    snprintf (col3v, 8, "Joy %d", gwiz.joy.srgt);
    column3 = GwizRenderText (col3v);
    d.w = column3->w; d.x = gic->window->w - 8 - column3->w;
    SDL_BlitSurface (column3, NULL, gic->window, &d);
    d.y += gwiz.font.height;
    FreeSurface (&column3);

    snprintf (col3v, 8, "Joy %d", gwiz.joy.fta);
    column3 = GwizRenderText (col3v);
    d.w = column3->w; d.x = gic->window->w - 8 - column3->w;
    SDL_BlitSurface (column3, NULL, gic->window, &d);
    d.y += gwiz.font.height;
    FreeSurface (&column3);

    snprintf (col3v, 8, "Joy %d", gwiz.joy.act);
    column3 = GwizRenderText (col3v);
    d.w = column3->w; d.x = gic->window->w - 8 - column3->w;
    SDL_BlitSurface (column3, NULL, gic->window, &d);
    d.y += gwiz.font.height;
    FreeSurface (&column3);

    snprintf (col3v, 8, "Joy %d", gwiz.joy.cancel);
    column3 = GwizRenderText (col3v);
    d.w = column3->w; d.x = gic->window->w - 8 - column3->w;
    SDL_BlitSurface (column3, NULL, gic->window, &d);
    d.y += gwiz.font.height;

    d.y = (gwiz.font.height + 8 + gwiz.font.height/2 - gwiz.cursor->h/2);
    d.y += (gwiz.font.height * gic->pos);
    d.x = 8;
    d.h = gwiz.cursor->h;
    d.w = gwiz.cursor->w;

    SDL_BlitSurface (gwiz.cursor, NULL, gic->window, &d);

    FreeSurface (&column3);

    FreeSurface (&column1);
    FreeSurface (&column2);
}

void GICListen (SDL_Event *e)
{
    char *msg = "Press a key or joybutton to remap";
    int button;
    if (e->type == SDL_JOYBUTTONDOWN)
	{
	    button = MsgBox (msg);
	    if (button == -1)
		return;
	    switch (gic->pos)
		{
		case 4:
		    gwiz.joy.slft = button;
		    break;
		case 5:
		    gwiz.joy.srgt = button;
		    break;
		case 6:
		    gwiz.joy.fta  = button;
		    break;
		case 7:
		    gwiz.joy.act  = button;
		    break;
		case 8:
		    gwiz.joy.cancel = button;
		    break;
		}
	    SaveJoyConfig();
	}
    if (e->type == SDL_KEYDOWN)
	{
	    button = MsgBox (msg);
	    if (button == -1)
		return;
	    if ((button == SDLK_UP) ||
		(button == SDLK_RIGHT) ||
		(button == SDLK_DOWN) ||
		(button == SDLK_LEFT) ||
		(button == SDLK_F11) ||
		(button == SDLK_F12) ||
		(button == SDLK_ESCAPE) ||
		(button == SDLK_RETURN))
		return;
	    switch (gic->pos)
		{
		case 0:
		    gwiz.key.fwd = button;
		    break;
		case 1:
		    gwiz.key.bkwd = button;
		    break;
		case 2:
		    gwiz.key.lft = button;
		    break;
		case 3:
		    gwiz.key.rgt = button;
		    break;
		case 4:
		    gwiz.key.slft = button;
		    break;
		case 5:
		    gwiz.key.srgt = button;
		    break;
		case 6:
		    gwiz.key.fta = button;
		    break;
		case 7:
		    gwiz.key.act = button;
		    break;
		case 8:
		    gwiz.key.cancel = button;
		    break;
		}
	}
}









