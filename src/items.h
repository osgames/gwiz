/*  items.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_ITEMS_H
#define HAVE_ITEMS_H

#include "items-camp.h"
#include "items-battle.h"

/* Loads an item, storing it in the given pointer's address */
void LoadItem (Inventory *item, int itemno);

/* Counts the number of available items */
int CountGwizItems (void);

void CompactPawnItems (PlayerPawn *pawn);

int FirstEmptyPawnItem (PlayerPawn *pawn);

int GetItemNoByName (char *name);

/* Checks to see if the item is of given type */
int ItemIsWeapon (Inventory *item);

int ItemIsArmor (Inventory *item);

int ItemIsShield (Inventory *item);

int ItemIsHelmet (Inventory *item);

int ItemIsGauntlet (Inventory *item);

int ItemIsMisc (Inventory *item);

int ItemIsScroll (Inventory *item);

/* Creates a linked list containing various item categories */
void MakeWeaponList (void);

void MakeArmorList (void);

void MakeShieldList (void);

void MakeHelmetList (void);

void MakeGauntletList (void);

void MakeMiscList (void);

void MakeScrollList (void);

void DropItem (GwizInspector *gi, PlayerPawn *pawn);

void DropMenuLoop (GwizInspector *gi, PlayerPawn *pawn);

void MoveDropCursor (PlayerPawn *pawn, int goingup);

void RequestDrop (PlayerPawn *pawn, Inventory *item);

int CountPawnItems (PlayerPawn *pawn);

void TradeItem (GwizInspector *gi, PlayerPawn *pawn);

int GetPawnItemPosition (GwizInspector *gi, PlayerPawn *pawn);

int MovePawnItemPositionCursor (GwizInspector *gi, PlayerPawn *pawn,
				int goingup);

void CleanPawnItemPositionCursor (GwizInspector *gi);

void UseItem (Inventory *item, void *origin, void *target);

void DestroyItemOnUse (PlayerPawn *pawn, Inventory *item);

#endif /* HAVE_ITEMS_H */
