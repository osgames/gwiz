/*  multiline.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <ctype.h>
#include "gwiz.h"
#include "text.h"
#include "multiline.h"
#include "uiloop.h"

extern GwizApp gwiz;

void NewMltm (short topalign, char *msg)
{
    SDL_Surface *window;
    SDL_Surface *displace;
    SDL_Rect wrect;
    SDL_Rect trect;
    char line[CHARSPERLINE+1];
    int linecount;
    int lpw = gwiz.canvas->h/2 - BORDERWIDTH*2;
    int oldoff = 0;
    int newoff = GetMltmEOL(msg, 0);
    int i;

    linecount = CountMltmLines(msg);

    trect.x = BORDERWIDTH;
    trect.y = BORDERHEIGHT;
    trect.h = gwiz.font.height;
    trect.w = gwiz.font.width*CHARSPERLINE;
    wrect.y = (topalign) ? 0 : gwiz.canvas->h/2;
    wrect.h = gwiz.canvas->h/2;
    wrect.w = gwiz.canvas->w;
    wrect.x = 0;

    window = NewTextBox (wrect.w - BORDERWIDTH*2, gwiz.canvas->h/2 - 
			 BORDERWIDTH*2);
    displace = NewGwizSurface (wrect.w, gwiz.canvas->h/2);
    SDL_BlitSurface (gwiz.canvas, &wrect, displace, NULL);
    SDL_BlitSurface (window, NULL, gwiz.canvas, &wrect);
    SDL_Flip (gwiz.canvas);

    for (i = 0; i < linecount + 1; i++)
	{
	    SDL_Surface *sline;

	    snprintf (line, newoff-oldoff+1, "%s", &msg[oldoff]);
	    sline = GwizRenderText (line);

	    trect.w = sline->w;
	    SDL_BlitSurface (sline, NULL, window, &trect);
	    SDL_BlitSurface (window, NULL, gwiz.canvas, &wrect);
	    trect.y += gwiz.font.height;
	    SDL_Flip (gwiz.canvas);

	    if ((i == linecount) ||
		(i == lpw))
		{
		    WaitForAnyKey();

		    trect.y = BORDERHEIGHT;
		    trect.h = lpw*gwiz.font.height;
		    trect.w = gwiz.canvas->w - BORDERWIDTH*2;
		    SDL_FillRect (window, &trect, gwiz.bgc);
		    trect.h = gwiz.font.height;
		}

	    oldoff = GetMltmBOL (msg, newoff);
	    if (oldoff == -1)
		break;
	    newoff = GetMltmEOL (msg, oldoff);
	    SDL_FreeSurface (sline);
	}
    SDL_BlitSurface (displace, NULL, gwiz.canvas, &wrect);
}

int CountMltmLines (char *msg)
{
    int lines = 1;
    int curcpl = GetMltmEOL (msg, 0);

    while ((curcpl = GetMltmEOL (msg, curcpl)) != -1)
	{
	    lines++;
	}

    return (lines);
}

int GetMltmEOL (char *msg, int offset)
{
    int oldoff = offset;
    int i;

    offset = GetMltmBOL (msg, offset);
    if (offset == -1)
	return (-1);
    for (i = offset; i < oldoff + CHARSPERLINE; i++)
	if (msg[i] == '\0')
	    return (-1);
    for (i = offset + CHARSPERLINE; i > oldoff; i--)
	{
	    if (isspace (msg[i]))
		break;
	    if (msg[i] == '\0')
		break;
	    if (i == oldoff)
		return (CHARSPERLINE);
	}
    return (i+1);
}

int GetMltmBOL (char *msg, int offset)
{
    int i;

    for (i = offset; i < CHARSPERLINE+offset; i++)
	if (isalnum(msg[i]) || (msg[i] == '\0'))
	    break;
    if (msg[i] == '\0')
	return (-1);
    return (i);
}


