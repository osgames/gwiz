/*  spells.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* Generally generic spell stuff. */
#ifndef HAVE_SPELLS_H
#define HAVE_SPELLS_H

#define NUM_SPELLS 64

/* the numbers are assigned for ease of use with the spell editor. */
typedef enum {
    IGNIS              = 0,  /* Halito - little flame */
    FERRUM_CORPORI     = 1,  /* Mogref - body iron */
    REQUIEM            = 2,  /* Katino - sleep */
    CLARITAS           = 3,  /* Dumapic - clarity */

    ALACRITAS          = 4,  /* Ponti - speed */
    SCINTILLATUS       = 5,  /* Melito - little sparks */
    RECLUDERE          = 6,  /* Desto - unlock */
    TERRERE            = 7,  /* Morlis - fear */
    CORDA_PETRI        = 8,  /* Bolatu - heart of stone */

    APERIRE            = 9,  /* Calific - reveal */
    IGNIS_POTENTI      = 10, /* Mahalito - large flames */
    FILTRUM_MAGICE     = 11, /* Cortu - magic screen */
    DISRUPTUS          = 12, /* Kantios - disruption */

    PUGNUS             = 13, /* Tzalik - fist */
    IGNIS_VEHEMENS     = 14, /* Lahalito - torch (violent burning) */
    VOLITARE           = 15, /* Litofeit - levitate */
    SENSU_PRIVARE      = 16, /* Rokdo - stun */

    ACCIRE             = 17, /* Socordi - summon */
    REX_GELI           = 18, /* Madalto - frost king */
    ARS_MAGICA_OBSTARE = 19, /* Bacortu - fizzle field */
    PARMAE_SOLVERE     = 20, /* Palios - dissolve fizzle field */
    ARCUS_PLUVIUS      = 21, /* Vaskyre - arch from rain */
    
    PARIAE_VIS         = 22, /* Mamogref - wall of force */
    EXIGERE            = 23, /* Zilwan - dispel */
    TERRAE_PASCET      = 24, /* Lokara - earth feast */
    PROCELLA_GELI      = 25, /* Ladalto - ice storm */
    
    LOCUS_NOVUS        = 26, /* Malor - teleport (change location) */
    OBTESTARI          = 27, /* Mahaman - beseech the gods */
    DIRUMPERE          = 28, /* Tiltowait - explode */
    CHAOS              = 29, /* Mawxiwtz - madhouse (chaos) */
    ARS_MAGICA_DEI     = 30, /* Abriel - divine magic */

/* End mage, begin clerical */

    MEDERI             = 31, /* Dios - heal */
    DAMNUM             = 32, /* Badios - harm */
    LUMEN              = 33, /* Milwa - light */
    BENEDICTUS         = 34, /* Kalki - blessings */
    PARMA              = 35, /* Porfic - shield */

    FASCINARE          = 36, /* Katu - charm */
    VISUS_EMENDATUS    = 37, /* Calfo - Xray */
    PLACIDUS_AER       = 38, /* Montino - still air */
    CORPUS_REPERIRE    = 39, /* Kandi - locate body */

    AGNOSCERE          = 40, /* Latumapic - identify */
    EXPERGERE          = 41, /* Dialko - awaken */
    PAX                = 42, /* Bamatu - peace */
    SOLIS_ORTUS        = 43, /* Lomilwa - sunbeam (sunrise) */
    MAGICE_SICCARE     = 44, /* Hakanido - magic drain */

    MEDERI_POTENTI     = 45, /* Dial - cure */
    SAUCIARE           = 46, /* Badial - wound */
    LAVABARE           = 47, /* Latumofis - cleanse */
    MAGNUS_PARMUM      = 48, /* Maporfic - big shield */
    VENTI_NOVACULA     = 49, /* Bariko - razor wind */

    MAGNUS_MEDERUM     = 50, /* Dialma - big cure */
    RENATUS            = 51, /* Di - life */
    INVOCARE           = 52, /* Bamordi - summoning */
    ASTRI_PORTA        = 53, /* Mogato - astral gate */
    INTERFICERE        = 54, /* Badi - loss of life */

    REVOCARE           = 55, /* Loktofeit - recall */
    SANARE             = 56, /* Madi - restore */
    FUR_VITAE          = 57, /* Labadi - life steal */
    VENTI_IGNIS        = 58, /* Kakamen - fire wind */

    VENTI_PETRUS       = 59, /* Mabariko - meteor wind */
    DIVINA_GRATIA      = 60, /* Ihalon - blessed favor (grace) */
    VENTI_MORS         = 61, /* Bakadi - death wind */
    INFERUS_EXCITARE   = 62, /* Kadorto - rebirth */

    NO_SPELL           = 63, /* retrospectively, should have been 0. :-\ */
} SpellNo;

typedef struct GwizSpell_ GwizSpell;

typedef enum {
    FIRST = 0,
    SECOND,
    THIRD,
    FOURTH,
    FIFTH,
    SIXTH,
    SEVENTH
} SpellLevel;

typedef enum {
    MAGE_CAMP = 0,
    CLERIC_CAMP = 1,
    MAGE_BATTLE = 2,
    CLERIC_BATTLE = 3,
    MAGE_BOTH = 4,
    CLERIC_BOTH = 5,
    MAGE_OTHER,
    CLERIC_OTHER
} SpellClass;

typedef enum {
    O_PAWN = 0,
    O_MONSTER,
    O_ITEM,
    O_NONE
} SpellOrigin;

typedef enum {
    T_PAWN = 0,
    T_PARTY,
    T_MONSTER,
    T_MPARTY,
    T_CLUSTER,
    T_HDOOR,
    T_NONE
} SpellTarget;

typedef enum {
    E_FIRE = 0,
    E_ICE,
    E_AIR,
    E_EARTH,
    E_NONE
} SpellElement;

typedef enum {
    EF_AC_ONE = 0,
    EF_AC_TWO,
    EF_AC_THREE,
    EF_AC_FOUR,
    EF_AC_FIVE,
    EF_AC_TEN,
    EF_STATUS,
    EF_RECOVERY,
    EF_SLEEP,
    EF_AGI,
    EF_FEAR,
    EF_TRAP,
    EF_MSCREEN,
    EF_DISRUPT,
    EF_STONE,
    EF_LIGHT,
    EF_LEVITATE,
    EF_STUN,
    EF_SUMMON,
    EF_FIZZLE,
    EF_DEFIZZLE,
    EF_RANDOM,
    EF_SWALLOW,
    EF_TELEPORT,
    EF_BESEECH,
    EF_CHARM,
    EF_XRAY,
    EF_LOCATEPAWN,
    EF_IDENTIFY,
    EF_AWAKEN,
    EF_LIGHT2,
    EF_MAGICDRAIN,
    EF_ANTIDOTE,
    EF_LIFE,
    EF_KILL,
    EF_FULLHEAL,
    EF_HPDRAIN,
    EF_FAVOR,
    EF_DCLUSTER,
    EF_REBIRTH,
    EF_NONE
} SpellEffect;

struct GwizSpell_ {
    SDL_Surface *visual;
    SpellLevel level;
    SpellClass class;
    SpellOrigin origintype;
    SpellTarget targettype;
    SpellElement elem;
    SpellNo spellno;
    SpellEffect effect;
    char name[19];
    int min;
    int max;
    void *origin;
    void *target;
};

#endif /* HAVE_SPELLS_H */
