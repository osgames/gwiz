/*  gwiz.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* Ha-ha; it's funny what too much uh...  I don't remember. */
#ifndef HAVE_GWIZ_CONFIG_H
#define HAVE_GWIZ_CONFIG_H

#include "config.h"
#endif

#include "spells.h"
#include "gwiz-pawn.h"
#include "shopnode.h"
#include "rotozoom.h"

#ifndef GWIZ_H
#define GWIZ_H

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define STONE    0x00000001
#define NWALL    0x00000002
#define EWALL    0x00000004
#define SWALL    0x00000008
#define WWALL    0x00000010
#define NDOOR    0x00000020
#define EDOOR    0x00000040
#define SDOOR    0x00000080
#define WDOOR    0x00000100
#define SPECIAL  0x00000200
#define NLDOOR   0x00000400
#define ELDOOR   0x00000800
#define SLDOOR   0x00001000
#define WLDOOR   0x00002000
#define NHDOOR   0x00004000
#define EHDOOR   0x00008000
#define SHDOOR   0x00010000
#define WHDOOR   0x00020000
#define TDARK    0x00040000
#define TELETO   0x00080000
#define TELEFROM 0x00100000
#define MAPPED   0x00200000

/* These may be renamed. */
#define DECO1    0x00400000
#define DECO2    0x00800000
#define DECO3    0x01000000
#define DECO4    0x02000000
#define DECO5    0x04000000
#define DECO6    0x08000000
#define DECO7    0x10000000
#define DECO8    0x20000000
#define DECO9    0x40000000
#define DECOA    0x80000000

#define NORTH 0
#define EAST 1
#define SOUTH 2
#define WEST 3

#define FWD 0
#define BKWD 1

#define WALL     (NWALL|EWALL|WWALL|SWALL)
#define DOOR     (NDOOR|EDOOR|WDOOR|SDOOR)

#define NUM_WALLS 22

typedef struct GwizApp_       GwizApp;
typedef struct GwizKeys_      GwizKeys;
typedef struct GwizUserData_  GwizUserData;
typedef struct GwizFont_      GwizFont;
typedef struct GwizMap_       GwizMap;
typedef struct GwizInspector_ GwizInspector;
typedef struct GwizStatusPix_ GwizStatusPix;
typedef struct GwizShopLists_ GwizShopLists;
typedef struct GwizJoystick_  GwizJoystick;
typedef struct GwizMEffects_  GwizMEffects;

struct GwizMEffects_ {
    unsigned char lumen;
    unsigned char volitare;
    unsigned char agnoscere;
};

struct GwizJoystick_ {
    SDL_Joystick *dev;
    int enabled;
    int whichdev;

    /* Unlike the keyconfig, these values represent sensitivity of the jAxis */
    int fwd;
    int lft;
    int rgt;
    int fta; /* "full turnabout" */

    int bkwd;
    int slft;
    int srgt;
    int act; /* "do this action" */
    int cancel; /* "exit this menu" */
};

struct GwizShopLists_ {
    GwizShopNode *wpn;
    GwizShopNode *arm;
    GwizShopNode *shld;
    GwizShopNode *hlm;
    GwizShopNode *gnt;
    GwizShopNode *misc;
    GwizShopNode *scrl;

    int wpncount;
    int armcount;
    int shldcount;
    int hlmcount;
    int gntcount;
    int misccount;
    int scrlcount;
};

struct GwizStatusPix_ {
    SDL_Surface *ok;
    SDL_Surface *poison;
    SDL_Surface *petrif;
    SDL_Surface *dead;
    SDL_Surface *ash;
    SDL_Surface *afraid;
    SDL_Surface *lost;
};

struct GwizInspector_ {
    SDL_Surface *area;
    SDL_Surface *slide;
    SDL_Rect dest;
    SDL_Rect name;
    SDL_Rect level;
    SDL_Rect sex;
    SDL_Rect ali;
    SDL_Rect class;
    SDL_Rect race;
    SDL_Rect ep;
    SDL_Rect age;
    SDL_Rect hp;
    SDL_Rect status;
    SDL_Rect attrs;
    SDL_Rect magic;
    SDL_Rect inven;
    SDL_Rect inven_e; /* equipped? */
    int whichpawn;
    int invenpos;
    int maxinvenpos;
};

struct GwizMap_ {
    int version;
    int npc[64];
    int tile[256][256];
    int music;
};

struct GwizFont_ {
    TTF_Font *face;
    int ptsize;
    int width;
    int height;
    int minx; /* unused */
    int maxx; /* unused */
    int miny; /* unused */
    int maxy; /* unused */
    int maxline;
    int aa;
};

struct GwizUserData_ {
    char *home;
    char *gwiz;
    char *cfg;
    char *party;
    char *post;
    char *monsters;
    char *items;
};

struct GwizKeys_ {
    int quit;
    int fwd;
    int bkwd;
    int lft;
    int rgt;
    int slft;
    int srgt;
    int fta; /* "full turnabout" */
    int act; /* "do this action" */
    int cancel; /* "exit this menu" */
};

struct GwizApp_ {
    SDL_Surface *canvas;
    SDL_Surface *wall[NUM_WALLS];
    SDL_Surface *floor[2];
    SDL_Surface *door[NUM_WALLS];
    SDL_Surface *tbord[8];
    /* just numbers.  0-25, all the same width */
    SDL_Surface *number[26];
    SDL_Surface *classpix[9];
    SDL_Surface *cursor;
    SDL_Surface *windowicon;

    PlayerPawn pawn[6];
    MonsterPawn mpawn[64]; /* The monsters that exist in this map. */
    GwizFont font;
    GwizKeys key;
    GwizUserData udata;
    GwizMap map;
    GwizInspector gi;
    GwizStatusPix status;
    GwizShopLists shop;
    GwizJoystick joy;
    GwizMEffects cond;
    
    Uint32 vidmode;
    Uint32 rmask;
    Uint32 gmask;
    Uint32 bmask;
    Uint32 amask;
    Uint32 nmask;
    Uint32 bgc; /* Background/foreground colors */
    Uint32 fgc;

    //INI *pf;

    int bpp;
    int pawnno[6];
    int shopstocks[256];
    int x;
    int y;
    int z;
    short face;
    char battle;
    char ffi; /* first floor image */
    char zaa; /* zoomed antialiasing? only available in 32bpp mode */
};

/* Combine *msg and *errcode.  Print both to stderr, and exit (1); */
void GErr (char *msg, ...);

/* Load the walls in a displayable format to render the maze */
void LoadWalls (void);

/* Load the doors in a displayable format to render the maze */
void LoadDoors (void);

/* malloc the length of *base and *file, return the path.  path can include
   PREFIX, for program install base directory.  Don't forget to Sfree() strings
   you make with this. */
char *MakePath (char *base, char *file);

/* Safe malloc.  Terminate with error output if the space was not allocated. */
void *Smalloc(size_t size);

/* Safe free.  Free space if it was malloc()ed, do nothing if (ptr == NULL) */
void Sfree(void *ptr);

/* Load the cursor to use with menus.  :) */
void LoadCursor (void);

void VerifyQuit (void);

int RandomRange (int min, int max);

void InitPawnNos(void);

void GwizShutdown (int exitcode);

void GwizSetWindowIcon (void);

void GwizGetOpts (int argc, char **argv);

SDL_Surface *NewGwizSurface (int width, int height);

/* Remember to free this string */
char *ItoA (int num, int places);

int CenterHoriz (SDL_Surface *first, SDL_Surface *second);

int CenterVert (SDL_Surface *first, SDL_Surface *second);

void WipeCanvas(void);

void UnloadWalls (void);

void UnloadDoors (void);

/* not as efficient as SDL_FreeSurface, so we only use it when it may not be
   safe to free a given surface; this will only free it if it exists. */
void FreeSurface (SDL_Surface **sfc);

int GwizRandom (int min, int max);

/* will be used (in the future, hopefully) as a way to locate a file in 
   $top_srcdir, to find files that have not been installed.  much work to be
   done on this.  FIXME */
char *GwizFindFile (char *fname); /* dynamically allocated */

#endif /* HAVE_GWIZ_H */



