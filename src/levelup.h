/*  levelup.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_LEVELUP_H
#define HAVE_LEVELUP_H

typedef struct LevelUpEvent_ LevelUpEvent;

struct LevelUpEvent_ {
    int hpinc;
    char *spell;
    int str;
    int iq;
    int dev;
    int vit;
    int agi;
    int lck;
    int mage_spell;
    int spell_candidates[5];
};

void HandleLevelUp (void);

int CheckLvUpXP (void);

int CheckLvUpExtendedXP (Uint32 threshold, int level);

void LvUpFig (void);

void LvUpMag (void);

void LvUpCle (void);

void LvUpThi (void);

void LvUpWiz (void);

void LvUpSam (void);

void LvUpLor (void);

void LvUpNin (void);

int IncreaseStrength (void);

int IncreaseIq (void);

int IncreaseDevotion (void);

int IncreaseVitality (void);

int IncreaseAgility (void);

int IncreaseLuck (void);

void SetAttrLimits (void);

int CalculateAttrLuck (void);

void EnforceMPRestrictions (void);

void LearnNewSpell (void);

#endif /* HAVE_LEVELUP_H */
