/*  gwiz.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include "gwiz.h"
#include "uiloop.h"
#include "prefsdat.h"
#include "text.h"
#include "maploader.h"
#include "menus.h"
#include "playerpawn.h"
#include "castle.h"
#include "playergen.h"
#include "inspect.h"
#include "joystick.h"

GwizApp gwiz;
const char *Usage = "\
GWiz, a GNU Wizards Game is available under the the GNU GPL, version 2 or\n\
later (at your option).\n\n\
Usage: %s [options]\n\n\
Options supported:\n\
  --antialias     | -a        \tAntialiased font (slower, prettier)\n\
  --depth <16|32> | -d <16|32>\tSpecify color depth\n\
  --fullscreen    | -f        \tRun in fullscreen mode\n\
  --help          | -h        \tPrint this help message\n\
  --joystick      | -j        \tEnable joystick use\n\
  --ptsize <size> | -p <size> \tSpecify the point size (12-16)\n\
  --solid         | -s        \tSolid font (faster, uglier)\n\
  --version       | -v        \tPrint version information\n\
  --window        | -w        \tRun in windowed mode\n\
\n";

int main (int argc, char **argv)
{
    char *invocation;

    invocation = strdup (argv[0]);

    InitPrefs(argc, argv);
    LoadPrefs();
    LoadJoyConfig();
    GwizGetOpts(argc, argv);
    
    /* This will have to be moved to GwizSanitizeOpts() or something */
    if (gwiz.font.ptsize != 16)
	fprintf(stderr, "Warning: -p flag unsupported.  If text overlaps, try with -p 16\n");
    
    /* Initialize the random number generator.  don't need anal
       randomness, so this is sufficient. */
    srand(time(NULL));
    
    if (gwiz.font.ptsize < 10)
	gwiz.font.ptsize = 16;
    if (gwiz.font.ptsize > 24)
	gwiz.font.ptsize = 24;
    
    Sfree (invocation);
    
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_JOYSTICK) != 0)
	GErr ("Unable to initialize SDL: %s", SDL_GetError());
    atexit (SDL_Quit);
    
    SDL_WM_SetCaption ("GWiz: A GNU Wizards Game", NULL);
    GwizSetWindowIcon();

    gwiz.canvas = SDL_SetVideoMode(800, 600, gwiz.bpp, gwiz.vidmode);
    if (gwiz.canvas == NULL)
	GErr ("Unable to set video mode: %s", SDL_GetError());

    InitGwizJoystick();
    InitPawnNos();
    InitGwizFonts();
    InitCommonNums();
    InitClassPix();
    InitStatusPix();
    LoadWalls();
    LoadDoors();
    LoadCursor();
    InitInspectEngine(&gwiz.gi);
    InitShopLists();
    MapLoad (1);
    atexit(SavePrefs);
    SaveJoyConfig(); /* make sure it can safely restore prefs */
    
    UILoop();
    
    /* we should never get this far. */
    return 1;
}

/* before you ask, "Bail" (i.e., jump ship), and "Call". "quit on demand" */
void GErr (char *msg, ...)
{
    va_list ap;
    char *p, *sval;
    int ival;
    double dval;

    va_start (ap, msg);
    for (p = msg; *p; p++)
	{
	    if (*p != '%')
		{
		    fputc (*p, stderr);
		    continue;
		}
	    switch (*++p)
		{
		case 'd':
		    ival = va_arg(ap, int);
		    fprintf (stderr, "%i", ival);
		    break;
		case 'f':
		    dval = va_arg(ap, double);
		    fprintf (stderr, "%f", dval);
		    break;
		case 's':
		    for (sval = va_arg(ap, char *); *sval; sval++)
			{
			    fputc (*sval, stderr);
			}
		    break;
		default:
		    fputc (*p, stderr);
		    break;
		}
	}

    fputc ('\n', stderr);

    GwizShutdown(1);
}

void LoadWalls (void)
{
    int i;

    gwiz.wall[21] = IMG_Load (PIXMAPSDIR "/wall11.png");
    gwiz.wall[20] = IMG_Load (PIXMAPSDIR "/wall10.png");
    gwiz.wall[19] = IMG_Load (PIXMAPSDIR "/wall9.png");
    gwiz.wall[17] = IMG_Load (PIXMAPSDIR "/wall8.png");
    gwiz.wall[16] = IMG_Load (PIXMAPSDIR "/wall7.png");
    
    gwiz.wall[18] = zoomSurface (gwiz.wall[17], 1.5, 1.0, gwiz.zaa);
    gwiz.wall[15] = zoomSurface (gwiz.wall[16], 1.5, 1.0, gwiz.zaa);

    gwiz.wall[14] = zoomSurface (gwiz.wall[19], 0.5, 0.5, gwiz.zaa);

    gwiz.wall[11] = zoomSurface (gwiz.wall[17], 0.5, 0.5, gwiz.zaa);
    gwiz.wall[12] = zoomSurface (gwiz.wall[11], 1.5, 1.0, gwiz.zaa);
    gwiz.wall[13] = zoomSurface (gwiz.wall[12], 1.5, 1.0, gwiz.zaa);

    gwiz.wall[10] = zoomSurface (gwiz.wall[16], 0.5, 0.5, gwiz.zaa);
    gwiz.wall[9]  = zoomSurface (gwiz.wall[10], 1.5, 1.0, gwiz.zaa);
    gwiz.wall[8]  = zoomSurface (gwiz.wall[9],  1.5, 1.0, gwiz.zaa);

    gwiz.wall[7]  = zoomSurface (gwiz.wall[19], 0.25, 0.25, gwiz.zaa);

    gwiz.wall[4]  = zoomSurface (gwiz.wall[11], 0.5, 0.5, gwiz.zaa);
    gwiz.wall[5]  = zoomSurface (gwiz.wall[4],  1.5, 1.0, gwiz.zaa);
    gwiz.wall[6]  = zoomSurface (gwiz.wall[5],  1.5, 1.0, gwiz.zaa);

    gwiz.wall[3]  = zoomSurface (gwiz.wall[10], 0.5, 0.5, gwiz.zaa);
    gwiz.wall[2]  = zoomSurface (gwiz.wall[3],  1.5, 1.0, gwiz.zaa);
    gwiz.wall[1]  = zoomSurface (gwiz.wall[2],  1.5, 1.0, gwiz.zaa);

    gwiz.wall[0]  = zoomSurface (gwiz.wall[19],  0.125, 0.125, gwiz.zaa);
 
    for (i = 0; i < NUM_WALLS; i++)
	if (gwiz.wall[i] == NULL)
	    GErr("Unable to create wall: %d", i);

    gwiz.floor[0] = IMG_Load (PIXMAPSDIR "/floor1.png");
    gwiz.floor[1] = IMG_Load (PIXMAPSDIR "/floor2.png");
}

void LoadDoors (void)
{
    int i;

    gwiz.door[21] = IMG_Load (PIXMAPSDIR "/door11.png");
    gwiz.door[20] = IMG_Load (PIXMAPSDIR "/door10.png");
    gwiz.door[19] = IMG_Load (PIXMAPSDIR "/door9.png");
    gwiz.door[17] = IMG_Load (PIXMAPSDIR "/door8.png");
    gwiz.door[16] = IMG_Load (PIXMAPSDIR "/door7.png");

    gwiz.door[18] = zoomSurface (gwiz.door[17], 1.5, 1.0, gwiz.zaa);
    gwiz.door[15] = zoomSurface (gwiz.door[16], 1.5, 1.0, gwiz.zaa);

    gwiz.door[14] = zoomSurface (gwiz.door[19], 0.5, 0.5, gwiz.zaa);

    gwiz.door[11] = zoomSurface (gwiz.door[17], 0.5, 0.5, gwiz.zaa);
    gwiz.door[12] = zoomSurface (gwiz.door[11], 1.5, 1.0, gwiz.zaa);
    gwiz.door[13] = zoomSurface (gwiz.door[12], 1.5, 1.0, gwiz.zaa);

    gwiz.door[10] = zoomSurface (gwiz.door[16], 0.5, 0.5, gwiz.zaa);
    gwiz.door[9]  = zoomSurface (gwiz.door[10], 1.5, 1.0, gwiz.zaa);
    gwiz.door[8]  = zoomSurface (gwiz.door[9],  1.5, 1.0, gwiz.zaa);

    gwiz.door[7]  = zoomSurface (gwiz.door[14], 0.5, 0.5, gwiz.zaa);

    gwiz.door[4]  = zoomSurface (gwiz.door[11], 0.5, 0.5, gwiz.zaa);
    gwiz.door[5]  = zoomSurface (gwiz.door[4],  1.5, 1.0, gwiz.zaa);
    gwiz.door[6]  = zoomSurface (gwiz.door[5],  1.5, 1.0, gwiz.zaa);

    gwiz.door[3]  = zoomSurface (gwiz.door[10], 0.5, 0.5, gwiz.zaa);
    gwiz.door[2]  = zoomSurface (gwiz.door[3],  1.5, 1.0, gwiz.zaa);
    gwiz.door[1]  = zoomSurface (gwiz.door[2],  1.5, 1.0, gwiz.zaa);

    gwiz.door[0]  = zoomSurface (gwiz.door[7],  0.5, 0.5, gwiz.zaa);
 
    for (i = 0; i < NUM_WALLS; i++)
	if (gwiz.door[i] == NULL)
	    GErr ("Unable to create door: %d", i);
}

char *MakePath (char *base, char *file)
{
    char *path;
    size_t pathlength = (strlen(base) + strlen(file) + 1);
    
    path = (char *)Smalloc (pathlength);
    if (path == NULL)
	GErr ("Unable to allocate memory for MakePath %s%s", base, file);
    strncpy (path, base, pathlength);
    strncat (path, file, pathlength-1-strlen(path));
    
    return (path);
}

void *Smalloc (size_t size)
{
    void *ptr;
    ptr = malloc(size);
    if (ptr == NULL)
	{
	    perror("malloc");
	    GwizShutdown(1);
	}
    return ptr;
}

void Sfree(void *ptr)
{
    if (ptr == NULL)
	return;
    free (ptr);
    /* FIXME should use a void **ptr as arg, and set the resulting pointer */
}

/* Unfortunately, I could not cleanly implement Srealloc() */

void LoadCursor (void)
{
    gwiz.cursor = IMG_Load (PIXMAPSDIR "/cursor.png");
    if (gwiz.cursor == NULL)
	GErr ("gwiz.c: Unable to open cursor pixmap %s", PIXMAPSDIR "/cursor.png");
}

void VerifyQuit (void)
{
    char *opts[] = {
	"Continue playing",
	"Quit game",
	NULL
    };
    if (NewGwizMenu(gwiz.canvas, opts, -1, -1, 0) == 1)
	GwizShutdown(0);
    else
	return;
}

void InitPawnNos(void)
{
    int i = 0;
    for (i = 0; i < 6; i++)
	{
	    gwiz.pawnno[i] = -1;
	    gwiz.pawn[i].ali = NEUTRAL;
	}
}

/* A "nice" shutdown call, that frees up a bunch of stuff before exiting */
void GwizShutdown (int exitcode)
{
    int i = 0;

    /* FIXME: Something in GwizShutdown sometimes causes a segfault. Looks like
       something is being freed before its time to go home! */
    /* FIXME: Save party. */
    for (i = 0; i < 6; i++)
	if (gwiz.pawnno[i] > -1)
	    {
		gwiz.pawn[i].saved_x = gwiz.x;
		gwiz.pawn[i].saved_y = gwiz.y;
		gwiz.pawn[i].saved_z = gwiz.z;
		SavePawn (gwiz.pawn[i], gwiz.pawnno[i]);
	    }
    if (gwiz.joy.dev); /*
	SaveJoyConfig();

    DestroyShopNodeList (&gwiz.shop.wpn);
    DestroyShopNodeList (&gwiz.shop.arm);
    DestroyShopNodeList (&gwiz.shop.shld);
    DestroyShopNodeList (&gwiz.shop.hlm);
    DestroyShopNodeList (&gwiz.shop.gnt);
    DestroyShopNodeList (&gwiz.shop.misc);
    DestroyShopNodeList (&gwiz.shop.scrl);

    FreeSurface (&gwiz.status.ok);
    FreeSurface (&gwiz.status.poison);
    FreeSurface (&gwiz.status.petrif);
    FreeSurface (&gwiz.status.dead);
    FreeSurface (&gwiz.status.ash);
    FreeSurface (&gwiz.status.afraid);
    FreeSurface (&gwiz.status.lost);

    FreeSurface (&gwiz.gi.area);
    FreeSurface (&gwiz.gi.slide);

    UnloadWalls();
    UnloadDoors();
    for (i = 0; i < 8; i++)
	FreeSurface (&gwiz.tbord[i]);
    for (i = 0; i < 26; i++)
	FreeSurface (&gwiz.number[i]);
    for (i = 0; i < 9; i++)
	FreeSurface (&gwiz.classpix[i]);
    FreeSurface (&gwiz.cursor);
    FreeSurface (&gwiz.windowicon);
    FreeSurface (&gwiz.canvas);
    
    TTF_CloseFont (gwiz.font.face);

    SaveINI (gwiz.pf);    
    Sfree (gwiz.udata.home);
    Sfree (gwiz.udata.gwiz);
    Sfree (gwiz.udata.cfg);
    Sfree (gwiz.udata.party);
    DestroyINI (gwiz.pf);
    */

    exit (exitcode);
}

void GwizSetWindowIcon (void)
{
    gwiz.windowicon = IMG_Load (PIXMAPSDIR "/gwiz_icon.xpm");
    if (gwiz.windowicon == NULL)
	fprintf(stderr, "Unable to load pixmap: %s\n", SDL_GetError());
    else
	SDL_WM_SetIcon (gwiz.windowicon, NULL);
}

void GwizGetOpts (int argc, char **argv)
{
    char *invocation;
    int next_option;
    const char *short_options = "ad:fhjp:svw";
    const struct option long_options[] = {
	{"antialias",  0, NULL, 'a'},
	{"depth",      1, NULL, 'd'},
	{"fullscreen", 0, NULL, 'f'},
	{"help",       0, NULL, 'h'},
	{"joystick",   0, NULL, 'j'},
	{"ptsize",     1, NULL, 'p'},
	{"solid",      0, NULL, 's'},
	{"version",    0, NULL, 'v'},
	{"window",     0, NULL, 'w'},
	{NULL,         0, NULL, 0}
    };

    invocation = strdup (argv[0]);

        do 
        {
	    next_option = getopt_long(argc, argv, short_options,
				      long_options, NULL);
	    switch (next_option)
		{
		case 'a':
		    gwiz.font.aa = 1;
		    break;
		case 'd':
		    gwiz.bpp = atoi(optarg);
		    if ((gwiz.bpp != 16) && (gwiz.bpp != 32))
			gwiz.bpp = 32;
		    break;
		case 'f':
		    gwiz.vidmode = (gwiz.vidmode | SDL_FULLSCREEN);
		    break;
		case 'h':
		    fprintf(stderr, Usage, invocation);
		    exit (0);
		    break;
		case 'j':
		    /* FIXME probably broke something by assuming no joy */
		    gwiz.joy.enabled = 1;
		    break;
		case 'p':
		    gwiz.font.ptsize = atoi(optarg);
		    break;
		case 's':
		    gwiz.font.aa = 0;
		    break;
		case 'v':
		    fprintf(stderr, "Gee Whiz version %s\n", VERSION);
		    exit(0);
		    break;
		case 'w':
		    gwiz.vidmode = (SDL_HWSURFACE | SDL_DOUBLEBUF);
		    break;
		case '?':
		case ':':
		    fprintf(stderr, Usage, invocation);
		    exit (0);
		    break;
		}
	} while (next_option != -1);

	Sfree (invocation);

	gwiz.zaa = (gwiz.bpp == 32) ? TRUE : FALSE;
}

SDL_Surface *NewGwizSurface (int width, int height)
{
    SDL_Surface *ret = NULL;

    ret = SDL_CreateRGBSurface (SDL_SWSURFACE, width, height, gwiz.bpp,
				gwiz.rmask, gwiz.gmask, gwiz.bmask, 
				gwiz.amask);
    ret = SDL_DisplayFormat (ret);
    SDL_FillRect (ret, NULL, gwiz.bgc);
    return (ret);
}

/* Remember to free this string */
char *ItoA (int num, int places)
{
    char string[places+1];

    snprintf (string, sizeof(char)*places, "%d", num);
    string[places+1] = '\0'; /* just to be safe */
    return (strdup (string));
}

int CenterHoriz (SDL_Surface *first, SDL_Surface *second)
{
    return (first->w/2 - second->w/2);
}

int CenterVert (SDL_Surface *first, SDL_Surface *second)
{
    return (first->h/2 - second->h/2);
}

void WipeCanvas(void)
{
    const int WIPE_BORDER_WIDTH = 5;
    SDL_Rect block;
    int maxiterations = (gwiz.canvas->w/2)/WIPE_BORDER_WIDTH -
	WIPE_BORDER_WIDTH*2;
    int i;

    block.x = gwiz.canvas->w/2 - WIPE_BORDER_WIDTH;
    block.y = gwiz.canvas->h/2 - WIPE_BORDER_WIDTH;
    block.h = WIPE_BORDER_WIDTH*2;
    block.w = WIPE_BORDER_WIDTH*2;

    for (i = 0; i < maxiterations; i++)
	{
	    SDL_FillRect (gwiz.canvas, &block, gwiz.bgc);
	    block.x -= WIPE_BORDER_WIDTH;
	    block.y -= WIPE_BORDER_WIDTH;
	    if (block.y < 0)
		block.y = 0;
	    block.h += WIPE_BORDER_WIDTH*2;
	    if (block.h > gwiz.canvas->h)
		block.h = gwiz.canvas->h;
	    block.w += WIPE_BORDER_WIDTH*2;

	    SDL_Flip (gwiz.canvas);
	}
}

void UnloadWalls (void)
{
    int i;
    for (i = 0; i < NUM_WALLS; i++)
	FreeSurface (&gwiz.wall[i]);

    FreeSurface (&gwiz.floor[0]);
    FreeSurface (&gwiz.floor[1]);
}

void UnloadDoors (void)
{
    int i;
    for (i = 0; i < NUM_WALLS; i++)
	FreeSurface (&gwiz.door[i]);
}

void FreeSurface (SDL_Surface **sfc)
{
    if (*sfc)
	SDL_FreeSurface (*sfc);
    *sfc = NULL;
}

int GwizRandom (int min, int max)
{
    return ((int) (max*rand()/(RAND_MAX + (1.0+min))));
}

char *GwizFindFile (char *fname) /* dynamically allocated */
{
    FILE *fd;
    static char *teh_file[FILENAME_MAX]; /* intentional typo, hehe */
    char *dirs[] = { "items",
		     "maps",
		     "monsters",
		     "pixmaps",
		     "sfx",
		     NULL
    };
    int i = 0;

    while (dirs[i] != NULL)
	{
	    snprintf ((char *)teh_file, FILENAME_MAX, "%s/%s", dirs[i], fname);
	    fd = fopen ((char *)teh_file, "r");
	    if (fd)
		{
		    fclose (fd);
		    return ((char *)teh_file);
		}
	    i++;
	}

    return (teh_file);
}

