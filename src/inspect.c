/*  inspect.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "gwiz.h"
#include "uiloop.h"
#include "prefsdat.h"
#include "text.h"
#include "maploader.h"
#include "menus.h"
#include "playerpawn.h"
#include "castle.h"
#include "playergen.h"
#include "party.h"
#include "inspect.h"
#include "items.h"
#include "equip.h"
#include "joystick.h"
#include "spellmanage.h"

extern GwizApp gwiz;

/* prime the inspection system by precaching common images and variables */
void InitInspectEngine(GwizInspector *gi)
{
	/* ok, this is b0rk, dunno why. see about 15 lines down */
    gi->area = NewTextBox (gwiz.font.width * 41, gwiz.font.height * 20);
    SetInspectorOverlays (gi);
    SetInspectorLabels (gi);
}

void NewGwizInspector(PlayerPawn *pawn, int mode, int whichpawn)
{
    GwizInspector gi;
    SDL_Event event;
    SDL_Rect dest;
    int breakout = FALSE;
    
    /* "solution" to b0rked InitInspectEngine */
    InitInspectEngine(&gi);
    
    SetInspectorGenerics (&gi, pawn);
    SetInspectorMajorCounters (&gi, pawn);
    SetInspectorMinorCounters (&gi, pawn);
    SetInspectorAttrCounters (&gi, pawn);
    SetInspectorOtherCounters (&gi, pawn);
    SetInspectorInventory(&gi, pawn);
    
    dest.x = gwiz.canvas->w/2 - gi.area->w/2;
    dest.y = gwiz.canvas->h/2 - gi.area->h/2;
    dest.h = gi.area->h;
    dest.w = gi.area->w;
    
    SDL_BlitSurface (gi.area, NULL, gwiz.canvas, &dest);
    SDL_Flip (gwiz.canvas);

    gi.whichpawn = whichpawn; /* no cleaner way to do this */
    
    while (SDL_WaitEvent(&event) != 0)
	{
	    SDL_Event *e = &event;
	    if (EventIsMisc (e))
		continue;
	    if (EventIsCancel (e))
		breakout = TRUE;
	    if (EventIsOk (e))
		InspectPawnMenu (&gi, pawn, mode);

	    SDL_BlitSurface (gi.area, NULL, gwiz.canvas, &dest);
	    SDL_Flip (gwiz.canvas);
	    if (breakout)
		break;
	}
    SDL_FreeSurface (gi.area);
}

void InspectPawn (int mode)
{
    int whichpawn = 0;
    SDL_Surface *screen = SDL_DisplayFormat (gwiz.canvas);
    
    switch (mode)
        {
	case 0: /* minipawnlist */
	    while ((whichpawn = NewMiniPawnList("Inspect who?")) != -1)
		{
		    NewGwizInspector (&gwiz.pawn[whichpawn], mode, whichpawn);
		}
	    break;
	case 1: /* full-size pawnlist */
	    while ((whichpawn = NewPawnList()) != -1)
		{
		    PlayerPawn pawn;
		    EmptyParty (); /* can't cast spells on people in party by
				      people who are not, trade items, etc. */
		    LoadPawn (whichpawn, &pawn);
		    NewGwizInspector (&pawn, mode, -1);
		    SavePawn (pawn, whichpawn);
		}
	    break;
	}
    SDL_BlitSurface (screen, NULL, gwiz.canvas, NULL);
    SDL_Flip (gwiz.canvas);
    SDL_FreeSurface (screen);
}

/* set the coordinates for various destinations */
void SetInspectorOverlays (GwizInspector *gi)
{
    int height = gwiz.font.height;
    int width = gwiz.font.width;
    gi->name.x = 8+width*5;
    gi->name.y = 8;
    gi->name.h = height;
    gi->name.w = width*15;
    
    gi->level.x = gi->name.x+gi->name.w + width*3;
    gi->level.y = gi->name.y;
    gi->level.h = height;
    gi->level.w = width*3;
    
    gi->sex.x = gi->level.x + gi->level.w + width;
    gi->sex.y = gi->level.y;
    gi->sex.h = height;
    gi->sex.w = width;
    
    gi->ali.x = gi->sex.x + gi->sex.w + width;
    gi->ali.y = gi->sex.y;
    gi->ali.h = height;
    gi->ali.w = width;
    
    gi->class.x = gi->ali.x + gi->ali.w + width;
    gi->class.y = gi->ali.y;
    gi->class.h = height;
    gi->class.w = width*3;
    
    gi->race.x = gi->area->w - 8 - width*6;
    gi->race.y = gi->class.y;
    gi->race.h = height;
    gi->race.w = width*6;
    
    gi->ep.x = gwiz.tbord[0]->h + 1 + width*6;
    gi->ep.y = 8+height;
    gi->ep.w = width*11;
    gi->ep.h = height*3;
    
    gi->attrs.x = 8 + width*9;
    gi->attrs.y = gi->ep.y + height * 4;
    gi->attrs.h = height*6;
    gi->attrs.w = width*2;
    
    gi->age.x = gi->area->w - 8 - width*3;
    gi->age.y = gi->ep.y;
    gi->age.w = width*2;
    gi->age.h = height*4;
    
    gi->hp.x = gi->area->w - 8 - width*9;
    gi->hp.y = gi->age.y + gi->age.h;
    gi->hp.h = height;
    gi->hp.w = width*9;
    
    gi->status.x = gi->hp.x;
    gi->status.y = gi->hp.y + gi->hp.h;
    gi->status.h = height;
    gi->status.w = width*9;
    
    gi->magic.x = gi->area->w - width*22 - 8;
    gi->magic.y = gi->status.y + gi->status.h;
    gi->magic.h = height*2;
    gi->magic.w = width*22;
    
    gi->inven.x = gwiz.tbord[0]->w + gwiz.cursor->w;
    gi->inven.y = gi->area->h - height*8 - gwiz.tbord[0]->h;;
    gi->inven.h = height*8;
    gi->inven.w = width*16;
}

/* common labels; draw/position them so the numbers can be copied quickly */
void SetInspectorLabels(GwizInspector *gi)
{
    SDL_Surface *name, *l, *ep, *age, *hp, *strength, *status;
    SDL_Rect d;
    char *eplist[] = { "E.P.", "G.P.", "Marks", NULL };
    char *agelist[] = { "Age", "AC", "Swim", "RIP", NULL };
    char *attrlist[] = { "Strength", "IQ", "Devotion", "Vitality",
			 "Agility", "Luck", NULL };
    
    name = GwizRenderText ("Name ");
    l = GwizRenderText ("L ");
    ep = NewVertTextMsg (eplist, 1);
    age = NewVertTextMsg (agelist, 1);
    strength = NewVertTextMsg (attrlist, 1);
    status = GwizRenderText ("Status");
    hp = GwizRenderText ("HP");
    
    d.x = 8; d.y = 8; d.h = gwiz.font.height; d.w = name->w;
    SDL_BlitSurface (name, NULL, gi->area, &d);
    
    d.x += name->w + gwiz.font.width*16;
    d.w = l->w;
    SDL_BlitSurface (l, NULL, gi->area, &d);
    
    d.x = 8; d.y += gwiz.font.height; d.w = ep->w;
    SDL_BlitSurface (ep, NULL, gi->area, &d);
    
    d.x = gi->area->w - 8 - gwiz.font.width*4 - age->w;
    d.w = age->w;
    SDL_BlitSurface (age, NULL, gi->area, &d);
    
    d.x = 8;
    d.y = 8 + gwiz.font.height*5;
    d.w = strength->w;
    SDL_BlitSurface (strength, NULL, gi->area, &d);
    
    d.x = gi->area->w - 8 - gwiz.font.width*10 - hp->w;
    d.w = hp->w;
    d.h = gwiz.font.height;
    SDL_BlitSurface (hp, NULL, gi->area, &d);
    
    d.x -= gwiz.font.width*4; d.y += gwiz.font.height;
    d.w = status->w;
    SDL_BlitSurface (status, NULL, gi->area, &d);
    
    SDL_FreeSurface (name);
    SDL_FreeSurface (l);
    SDL_FreeSurface (ep);
    SDL_FreeSurface (age);
    SDL_FreeSurface (strength);
    SDL_FreeSurface (status);
    SDL_FreeSurface (hp);
}

/* The top row with generally useful information */
void SetInspectorGenerics(GwizInspector *gi, PlayerPawn *pawn)
{
    char name[16];
    char level[4];
    char sex[2];
    char ali[2];
    char class[4];
    char race [7];
    SDL_Surface *fields[6];
    int i = 0;
    
    strncpy (name, pawn->name, 16);
    snprintf(level, sizeof(char)*4, "%-3d", pawn->level);
    
    if (pawn->sex == MALE)
	strncpy (sex, "M", 2);
    else
	strncpy (sex, "F", 2);
    
    if (pawn->ali == GOOD)
	strncpy (ali, "G", 2);
    else if (pawn->ali == NEUTRAL)
	strncpy (ali, "N", 2);
    else
	strncpy (ali, "E", 2);
    
    GetClassAbbr(pawn->class, class);
    
    switch (pawn->race)
        {
	case HUMAN:
	    strncpy (race, "Human", 6);
	    break;
	case ELF:
	    strncpy (race, "Elf", 4);
	    break;
	case GNOME:
	    strncpy (race, "Gnome", 6);
	    break;
	case DWARF:
	    strncpy (race, "Dwarf", 6);
	    break;
	case HOBBIT:
	    strncpy (race, "Hobbit", 7);
	    break;
	}
    
    fields[0] = GwizRenderText (name);
    fields[1] = GwizRenderText (level);
    fields[2] = GwizRenderText (sex);
    fields[3] = GwizRenderText (ali);
    fields[4] = GwizRenderText (class);
    fields[5] = GwizRenderText (race);
    
    SDL_BlitSurface (fields[0], NULL, gi->area, &gi->name);
    SDL_BlitSurface (fields[1], NULL, gi->area, &gi->level);
    SDL_BlitSurface (fields[2], NULL, gi->area, &gi->sex);
    SDL_BlitSurface (fields[3], NULL, gi->area, &gi->ali);
    SDL_BlitSurface (fields[4], NULL, gi->area, &gi->class);
    SDL_BlitSurface (fields[5], NULL, gi->area, &gi->race);
    
    for (i = 0; i < 6; i++)
	SDL_FreeSurface (fields[i]);
}

/* Experience, Gold, Marks.  Things that have lots of space. */
void SetInspectorMajorCounters (GwizInspector *gi, PlayerPawn *pawn)
{
    char ep[12];
    char gp[12];
    char marks[12];
    SDL_Surface *area;
    SDL_Surface *fields[3];
    SDL_Rect d;
    
    snprintf (ep, sizeof(char)*12, "%lu", pawn->counter.ep);
    snprintf (gp, sizeof(char)*12, "%lu", pawn->counter.gp);
    snprintf (marks, sizeof(char)*12, "%d", pawn->counter.marks);

    fields[0] = GwizRenderText (ep);
    fields[1] = GwizRenderText (gp);
    fields[2] = GwizRenderText (marks);
    
    area = NewGwizSurface (gwiz.font.width*11, gwiz.font.height*3);
    if (area == NULL)
	GErr ("inspect.c: unable to create area:%s ", SDL_GetError());
    
    d.x = area->w - fields[0]->w;;
    d.y = 0; 
    d.h = gwiz.font.height;
    d.w = fields[0]->w;
    
    SDL_BlitSurface (fields[0], NULL, area, &d);
    d.y += gwiz.font.height;
    d.w = fields[1]->w;
    d.x = area->w - fields[1]->w;
    SDL_BlitSurface (fields[1], NULL, area, &d);
    d.y += gwiz.font.height;
    d.w = fields[2]->w;
    d.x = area->w - fields[2]->w;
    SDL_BlitSurface (fields[2], NULL, area, &d);
    
    SDL_FreeSurface (fields[0]);
    SDL_FreeSurface (fields[1]);
    SDL_FreeSurface (fields[2]);
    
    SDL_BlitSurface (area, NULL, gi->area, &gi->ep);
    SDL_FreeSurface (area);
}

/* Age, AC, Swim, RIP. things that do not generally go very far. */
void SetInspectorMinorCounters (GwizInspector *gi, PlayerPawn *pawn)
{
    char age[3];
    char ac[3];
    char rip[3];
    SDL_Surface *fields[3];
    SDL_Surface *area;
    SDL_Rect d;
    
    d.x = 0; d.y = 0; d.h = gwiz.font.height; d.w = gwiz.font.width*2;
    
    area = NewGwizSurface (gwiz.font.width*3, gwiz.font.height*4);
    if (area == NULL)
	GErr ("inspect.c: unable to create area: %s",
		  SDL_GetError());
    
    snprintf (age, sizeof(char)*3, "%d", pawn->counter.age);
    snprintf (ac, sizeof(char)*3, "%d", pawn->attr.ac);
    snprintf (rip, sizeof(char)*3, "%d", pawn->counter.rip);
    
    fields[0] = GwizRenderText (age);
    fields[1] = GwizRenderText (ac);
    fields[2] = GwizRenderText (rip);
    
    SDL_BlitSurface (fields[0], NULL, area, &d);
    d.y += gwiz.font.height;
    SDL_BlitSurface (fields[1], NULL, area, &d);
    d.y += gwiz.font.height;
    SDL_BlitSurface (gwiz.number[pawn->attr.swim], NULL, area, &d);
    d.y += gwiz.font.height;
    SDL_BlitSurface (fields[2], NULL, area, &d);
    
    SDL_BlitSurface (area, NULL, gi->area, &gi->age);
    SDL_FreeSurface (area);
    SDL_FreeSurface (fields[0]);
    SDL_FreeSurface (fields[1]);
    SDL_FreeSurface (fields[2]);
}

/* Generic attributes. */
void SetInspectorAttrCounters (GwizInspector *gi, PlayerPawn *pawn)
{
    SDL_Surface *area;
    SDL_Rect d;
    
    d.x = 0; d.y = 0; d.h = gwiz.font.height; d.w = gwiz.font.width*2;
    
    area = NewGwizSurface (gwiz.font.width*2, gwiz.font.height*6);
    if (area == NULL)
	GErr ("inspect.c: unable to create area: %s",
		  SDL_GetError());
    
    SDL_BlitSurface (gwiz.number[pawn->attr.str], NULL, area, &d);
    d.y += gwiz.font.height;
    SDL_BlitSurface (gwiz.number[pawn->attr.iq], NULL, area, &d);
    d.y += gwiz.font.height;
    SDL_BlitSurface (gwiz.number[pawn->attr.dev], NULL, area, &d);
    d.y += gwiz.font.height;
    SDL_BlitSurface (gwiz.number[pawn->attr.vit], NULL, area, &d);
    d.y += gwiz.font.height;
    SDL_BlitSurface (gwiz.number[pawn->attr.agi], NULL, area, &d);
    d.y += gwiz.font.height;
    SDL_BlitSurface (gwiz.number[pawn->attr.luck], NULL, area, &d);
    
    SDL_BlitSurface (area, NULL, gi->area, &gi->attrs);
    SDL_FreeSurface (area);
}

/* HP, Magic. */
void SetInspectorOtherCounters (GwizInspector *gi, PlayerPawn *pawn)
{
    char hp[10];
    char mm[23];
    char cm[23];
    SDL_Surface *fields[4];
    SDL_Rect d;
    
    snprintf (hp, sizeof(char)*10, "%-3d/%-3d", pawn->counter.hp,
	      pawn->counter.hp_max);
    snprintf (mm, sizeof(char)*23, "M %-2d/%-2d/%-2d/%-2d/%-2d/%-2d/%d",
	      pawn->mm.spellpoints[0], pawn->mm.spellpoints[1],
	      pawn->mm.spellpoints[2], pawn->mm.spellpoints[3],
	      pawn->mm.spellpoints[4], pawn->mm.spellpoints[5],
	      pawn->mm.spellpoints[6]);
    snprintf (cm, sizeof(char)*23, "C %-2d/%-2d/%-2d/%-2d/%-2d/%-2d/%d",
	      pawn->cm.spellpoints[0], pawn->cm.spellpoints[1],
	      pawn->cm.spellpoints[2], pawn->cm.spellpoints[3],
	      pawn->cm.spellpoints[4], pawn->cm.spellpoints[5],
	      pawn->cm.spellpoints[6]);
    
    switch (pawn->status & 0xffffffff) /* "All ailments" */
	{
	case STATUS_OK:
	    SDL_BlitSurface (gwiz.status.ok, NULL, gi->area, &gi->status);
	    break;
	case STATUS_POISON:
	    SDL_BlitSurface (gwiz.status.poison, NULL, gi->area,
			     &gi->status);
	    break;
	case STATUS_PETRIF:
	    SDL_BlitSurface (gwiz.status.petrif, NULL, gi->area,
			     &gi->status);
	    break;
	case STATUS_DEAD:
	    SDL_BlitSurface (gwiz.status.dead, NULL, gi->area,
			     &gi->status);
	    break;
	case STATUS_ASH:
	    SDL_BlitSurface(gwiz.status.ash, NULL, gi->area, &gi->status);
	    break;
	case STATUS_AFRAID:
	    SDL_BlitSurface(gwiz.status.afraid, NULL, gi->area,
			    &gi->status);
	    break;
	case STATUS_LOST:
	    SDL_BlitSurface(gwiz.status.lost, NULL, gi->area,
			    &gi->status);
	    break;
	}
    
    d.x = 0; d.y = 0; d.h = gwiz.font.height; d.w = gwiz.font.width*22;
    
    fields[0] = GwizRenderText(hp);
    fields[1] = NewGwizSurface (gwiz.font.width*22, gwiz.font.height*2);
    if (fields[1] == NULL)
	GErr ("inspect.c: unable to create fields[1]: %s",
		  SDL_GetError());
    
    fields[2] = GwizRenderText (mm);
    fields[3] = GwizRenderText (cm);
    
    SDL_BlitSurface (fields[2], NULL, fields[1], &d);
    d.y += gwiz.font.height;
    SDL_BlitSurface (fields[3], NULL, fields[1], &d);
    SDL_BlitSurface (fields[0], NULL, gi->area, &gi->hp);
    SDL_BlitSurface (fields[1], NULL, gi->area, &gi->magic);
    
    SDL_FreeSurface (fields[0]);
    SDL_FreeSurface (fields[1]);
    SDL_FreeSurface (fields[2]);
    SDL_FreeSurface (fields[3]);
}

/* Inventory */
void SetInspectorInventory (GwizInspector *gi, PlayerPawn *pawn)
{
    SDL_Surface *area;
    SDL_Surface *asterisk;
    SDL_Surface *hash;
    SDL_Surface *question;
    SDL_Surface *hyphen;
    SDL_Rect dest;
    int i;

    dest.x = 0;
    dest.y = 0;
    dest.h = gwiz.font.height;
    dest.w = gwiz.font.width;

    CompactPawnItems (pawn);

    area = NewGwizSurface (gwiz.font.width*16, gwiz.font.height*8);
    asterisk = GwizRenderText ("*");
    hash = GwizRenderText ("#");
    question = GwizRenderText ("?");
    hyphen = GwizRenderText ("-");

    for (i = 0; i < 8; i++)
	if (pawn->item[i].version > -1)
	    {
		SDL_Surface *name = GwizRenderText (pawn->item[i].name);

		/* Unidentified should be the last surface drawn when
		   applicable, preceded by non-equippable, followed by
		   equipped, and finally cursed.  This order is important. */
		if ((pawn->item[i].special & CURSED) == CURSED)
		    SDL_BlitSurface (hyphen, NULL, area, &dest);
		if ((pawn->item[i].usage & USAGE_EQUIPPED) == USAGE_EQUIPPED)
		    SDL_BlitSurface (asterisk, NULL, area, &dest);
		if ((!PawnItemCompatible(pawn->class, pawn->item[i].usage)) &&
		    (pawn->item[i].usage != USAGE_NONE))
		    SDL_BlitSurface (hash, NULL, area, &dest);
		/* FIXME:  Unidentified items */
		dest.x += gwiz.font.width;
		dest.w = gwiz.font.width*15;

		SDL_BlitSurface (name, NULL, area, &dest);
		dest.y += gwiz.font.height;
		dest.x = 0;
		dest.w = gwiz.font.width;
	    }
    SDL_BlitSurface (area, NULL, gi->area, &gi->inven);

    SDL_FreeSurface (area);
    SDL_FreeSurface (asterisk);
    SDL_FreeSurface (hash);
    SDL_FreeSurface (hyphen);
    SDL_FreeSurface (question);
}

void InspectPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode)
{
    switch (pawn->class)
	{
	case FIGHTER:
	    InspectFighterPawnMenu (gi, pawn, mode);
	    break;
	case MAGE:
	    InspectMagePawnMenu (gi, pawn, mode);
	    break;
	case CLERIC:
	    InspectClericPawnMenu (gi, pawn, mode);
	    break;
	case WIZARD:
	    InspectWizardPawnMenu (gi, pawn, mode);
	    break;
	case THIEF:
	    InspectThiefPawnMenu (gi, pawn, mode);
	    break;
	case SAMURAI:
	    InspectSamuraiPawnMenu (gi, pawn, mode);
	    break;
	case LORD:
	    InspectLordPawnMenu (gi, pawn, mode);
	    break;
	case NINJA:
	    InspectNinjaPawnMenu (gi, pawn, mode);
	    break;
	}
}

void InspectFighterPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode)
{
    int option = 0;
    char *labels[] = {
	"Equip",
	"Trade",
	"(D) Use",
	"Pool Gold",
	"Read",
	"Drop",
	"Leave",
	NULL
    };
    
    while ((option = NewGwizMenu(gi->area, labels, gi->area->w, 
				 gi->area->h, option)) != -1)
	{
	    switch (option)
		{
		case 0:
		    Equip (gi, pawn);
		    break;
		case 1:
		    TradeItem (gi, pawn);
		    break;
		case 2:
		    break;
		case 3:
		    PoolGold (gi->whichpawn);
		    if (gi->whichpawn > -1)
			GwizInspectorMajorCounterUpdate (gi, pawn);
		    break;
		case 4:
		    ReadSpells (pawn);
		    break;
		case 5:
		    DropItem (gi, pawn);
		    break;
		case 6:
		    return;
		}
	}
}

void InspectMagePawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode)
{
    int option = 0;
    char *labels[] = {
	"(D) Spell",
	"Equip",
	"Trade",
	"(D) Use",
	"Pool Gold",
	"Read",
	"Drop",
	"Leave",
	NULL
    };
    while ((option = NewGwizMenu(gi->area, labels, gwiz.canvas->w, 
				 gwiz.canvas->h, option)) != -1)
	{
	    switch (option)
		{
		case 0:
		    break;
		case 1:
		    Equip (gi, pawn);
		    break;
		case 2:
		    TradeItem (gi, pawn);
		    break;
		case 3:
		    break;
		case 4:
		    PoolGold (gi->whichpawn);
		    if (gi->whichpawn > -1)
			GwizInspectorMajorCounterUpdate (gi, pawn);
		    break;
		case 5:
		    ReadSpells (pawn);
		    break;
		case 6:
		    DropItem (gi, pawn);
		    break;
		case 7:
		    return;
		}
	}
}

void InspectClericPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode)
{
    int option = 0;
    char *labels[] = {
	"(D) Spell",
	"Equip",
	"Trade",
	"(D) Use",
	"Pool Gold",
	"Read",
	"Drop",
	"Leave",
	NULL
    };
    while ((option = NewGwizMenu(gi->area, labels, gwiz.canvas->w, 
				 gwiz.canvas->h, option)) != -1)
	{
	    switch (option)
		{
		case 0:
		    break;
		case 1:
		    Equip (gi, pawn);
		    break;
		case 2:
		    TradeItem (gi, pawn);
		    break;
		case 3:
		    break;
		case 4: 
		    PoolGold (gi->whichpawn);
		    if (gi->whichpawn > -1)
			GwizInspectorMajorCounterUpdate (gi, pawn);
		    break;
		case 5:
		    ReadSpells (pawn);
		    break;
		case 6:
		    DropItem (gi, pawn);
		    break;
		case 7:
		    return;
		}
	}
}

void InspectWizardPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode)
{
    int option = 0;
    char *labels[] = {
	"(D) Spell",
	"Equip",
	"Trade",
	"(D) Identify",
	"(D) Use",
	"Pool Gold",
	"Read",
	"Drop",
	"Leave",
	NULL
    };
    while ((option = NewGwizMenu(gi->area, labels, gwiz.canvas->w, 
				 gwiz.canvas->h, option)) != -1)
	{
	    switch (option)
		{
		case 0:
		    break;
		case 1:
		    Equip (gi, pawn);
		    break;
		case 2:
		    TradeItem (gi, pawn);
		    break;
		case 3:
		    break;
		case 4:
		    break;
		case 5:
		    PoolGold (gi->whichpawn);
		    if (gi->whichpawn > -1)
			GwizInspectorMajorCounterUpdate (gi, pawn);
		    break;
		case 6:
		    ReadSpells (pawn);
		    break;
		case 7:
		    DropItem (gi, pawn);
		    break;
		case 8:
		    return;
		}
	}
}

void InspectThiefPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode)
{
    int option = 0;
    char *labels[] = {
	"Equip",
	"Trade",
	"(D) Use",
	"Pool Gold",
	"Read",
	"Drop",
	"Leave",
	NULL
    };
    while ((option = NewGwizMenu(gi->area, labels, gwiz.canvas->w, 
				 gwiz.canvas->h, option)) != -1)
	{
	    switch (option)
		{
		case 0:
		    Equip (gi, pawn);
		    break;
		case 1: 
		    TradeItem (gi, pawn);
		    break;
		case 2:
		    break;
		case 3: 
		    PoolGold (gi->whichpawn);
		    if (gi->whichpawn > -1)
			GwizInspectorMajorCounterUpdate (gi, pawn);
		    break;
		case 4:
		    ReadSpells (pawn);
		    break;
		case 5:
		    DropItem (gi, pawn);
		    break;
		case 6:
		    return;
		}
	}
}

void InspectSamuraiPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode)
{
    int option = 0;
    char *labels[] = {
	"(D) Spell",
	"Equip",
	"Trade",
	"(D) Use",
	"Pool Gold",
	"Read",
	"Drop",
	"Leave",
	NULL
    };
    while ((option = NewGwizMenu(gi->area, labels, gwiz.canvas->w, 
				 gwiz.canvas->h, option)) != -1)
	{
	    switch (option)
		{
		case 0:
		    break;
		case 1:
		    Equip (gi, pawn);
		    break;
		case 2:
		    TradeItem (gi, pawn);
		    break;
		case 3:
		    break;
		case 4:
		    PoolGold (gi->whichpawn);
		    if (gi->whichpawn > -1)
			GwizInspectorMajorCounterUpdate (gi, pawn);
		    break;
		case 5:
		    ReadSpells (pawn);
		    break;
		case 6:
		    DropItem (gi, pawn);
		    break;
		case 7:
		    return;
		}
	}
}

void InspectLordPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode)
{
    int option = 0;
    char *labels[] = {
	"(D) Spell",
	"Equip",
	"Trade",
	"(D) Use",
	"Pool Gold",
	"Read",
	"Drop",
	"Leave",
	NULL
    };
    while ((option = NewGwizMenu(gi->area, labels, gwiz.canvas->w, 
				 gwiz.canvas->h, option)) != -1)
	{
	    switch (option)
		{
		case 0:
		    break;
		case 1:
		    Equip (gi, pawn);
		    break;
		case 2:
		    TradeItem (gi, pawn);
		    break;
		case 3:
		    break;
		case 4:
		    PoolGold (gi->whichpawn);
		    if (gi->whichpawn > -1)
			GwizInspectorMajorCounterUpdate (gi, pawn);
		    break;
		case 5:
		    ReadSpells (pawn);
		    break;
		case 6:
		    DropItem (gi, pawn);
		    break;
		case 7:
		    return;
		}
	}
}

void InspectNinjaPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode)
{
    int option = 0;
    char *labels[] = {
	"Equip",
	"Trade",
	"(D) Use",
	"Pool Gold",
	"Read",
	"Drop",
	"Leave",
	NULL
    };
    while ((option = NewGwizMenu(gi->area, labels, gwiz.canvas->w, 
				 gwiz.canvas->h, option)) != -1)
	{
	    switch (option)
		{
		case 0:
		    Equip (gi, pawn);
		    break;
		case 1: 
		    TradeItem (gi, pawn);
		    break;
		case 2:
		    break;
		case 3:
		    PoolGold (gi->whichpawn);
		    if (gi->whichpawn > -1)
			GwizInspectorMajorCounterUpdate (gi, pawn);
		    break;
		case 4:
		    ReadSpells (pawn);
		    break;
		case 5:
		    DropItem (gi, pawn);
		    break;
		case 6:
		    return;
		}
	}
}

void GwizInspectorMajorCounterUpdate (GwizInspector *gi, PlayerPawn *pawn)
{
    SDL_Rect dest;

    if (gi->whichpawn < 0)
	return; /* don't execute if there's no party */

    dest.x = gwiz.canvas->w/2 - gi->area->w/2;
    dest.y = gwiz.canvas->h/2 - gi->area->h/2;
    dest.h = gi->area->h;
    dest.w = gi->area->w;

    SetInspectorMajorCounters (gi, pawn);
    SDL_BlitSurface (gi->area, NULL, gwiz.canvas, &dest);
    SDL_Flip (gwiz.canvas);
}
