/*  enums.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_ENUMS_H
#define HAVE_ENUMS_H

typedef enum {
    HUMAN_MINSTR   = 8,
    HUMAN_MAXSTR   = 18,
    HUMAN_MINIQ    = 8,
    HUMAN_MAXIQ    = 18,
    HUMAN_MINDEV   = 5,
    HUMAN_MAXDEV   = 15,
    HUMAN_MINVIT   = 8,
    HUMAN_MAXVIT   = 18,
    HUMAN_MINAGI   = 8,
    HUMAN_MAXAGI   = 18,
    HUMAN_MINLUCK  = 9,
    HUMAN_MAXLUCK  = 19,

    ELF_MINSTR     = 7,
    ELF_MAXSTR     = 17,
    ELF_MINIQ      = 10,
    ELF_MAXIQ      = 20,
    ELF_MINDEV     = 10,
    ELF_MAXDEV     = 20,
    ELF_MINVIT     = 6,
    ELF_MAXVIT     = 16,
    ELF_MINAGI     = 9,
    ELF_MAXAGI     = 19,
    ELF_MINLUCK    = 6,
    ELF_MAXLUCK    = 16,

    DWARF_MINSTR   = 10,
    DWARF_MAXSTR   = 20,
    DWARF_MINIQ    = 7,
    DWARF_MAXIQ    = 17,
    DWARF_MINDEV   = 10,
    DWARF_MAXDEV   = 20,
    DWARF_MINVIT   = 10,
    DWARF_MAXVIT   = 20,
    DWARF_MINAGI   = 5,
    DWARF_MAXAGI   = 15,
    DWARF_MINLUCK  = 6,
    DWARF_MAXLUCK  = 16,

    GNOME_MINSTR   = 7,
    GNOME_MAXSTR   = 17,
    GNOME_MINIQ    = 7,
    GNOME_MAXIQ    = 17,
    GNOME_MINDEV   = 10,
    GNOME_MAXDEV   = 20,
    GNOME_MINVIT   = 8,
    GNOME_MAXVIT   = 18,
    GNOME_MINAGI   = 10,
    GNOME_MAXAGI   = 20,
    GNOME_MINLUCK  = 7,
    GNOME_MAXLUCK  = 17,

    HOBBIT_MINSTR  = 5,
    HOBBIT_MAXSTR  = 15,
    HOBBIT_MINIQ   = 7,
    HOBBIT_MAXIQ   = 17,
    HOBBIT_MINDEV  = 7,
    HOBBIT_MAXDEV  = 17,
    HOBBIT_MINVIT  = 6,
    HOBBIT_MAXVIT  = 16,
    HOBBIT_MINAGI  = 12,
    HOBBIT_MAXAGI  = 22,
    HOBBIT_MINLUCK = 15,
    HOBBIT_MAXLUCK = 25
} PawnLimits;

#endif /* HAVE_ENUMS_H */
