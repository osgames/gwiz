#ifndef USING_GINI_H
#define USING_GINI_H

void ConfCreate (char *in_name, char *out_name);

int ConfExists (char *fname);

void OpenConf (char *fname);

void SaveConf (char *fname);

void ParseConfOpt (char *line);

#endif /* USING_GINI_H */
