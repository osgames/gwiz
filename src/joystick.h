/*  joystick.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_JOYSTICK_H
#define HAVE_JOYSTICK_H

typedef struct GwizJoyConfigWindow_ GwizJoyConfigWindow;

struct GwizJoyConfigWindow_ {
    SDL_Surface *area;
    SDL_Surface *joylist;

    SDL_Surface *displace;
    SDL_Rect drect;
    SDL_Rect crect;
    SDL_Rect brect;
    int position;
    int maxposition;
};

void InitGwizJoystick (void);

void GwizOpenJoystick (int jsno);

void QuitGwizJoystick (void);

void CheckJoy (void);

void JoyConfig (void);

void UpdateJoyMappingDisplay (void);

void ChooseJoystick (void);

void PrepJoyConfig (void);

void JoyConfigLoop (void);

void MoveJoyConfigCursor (SDL_Surface *surface, int goingup);

int SetJoyCursorFromMouse (SDL_Event *event);

int GetJoyButtonNo (void);

/* seperate from prefsdat because that format will break a lot. configuring 
   joysticks is a pain in the ass, so I'll only make you do it once. */
void SaveJoyConfig (void);

void LoadJoyConfig (void);

int JoyAxisMotion (SDL_Event *event, int direction);

Uint8 GetJoyButton (SDL_Event *event);

#endif /* HAVE_JOYSTICK_H */
