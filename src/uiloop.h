/*  uiloop.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef UI_LOOP_H
#define UI_LOOP_H

enum {
    OBJ_FRONT,
    OBJ_LEFT,
    OBJ_RIGHT
};

/* The main event loop.  not a "real" event loop.  FIXME: Make me an event
   loop. */
void UILoop (void);

/* Checks for keys currently .  Do action for a given key if necessary */
void ScanKeys(void);

char CheckMapObject (int type, short pos, short row, short col);

/* Change the direction the player is facing, 0 = left turn, 1 = right turn */
void SetFace (int turn);

/* Attempt to move player pawn */
char MovePlayer (int direction);

/* Slide left or right.  0 = left, 1 = right */
char SlidePlayer (int direction);

/* Kick doors open/advance */
char Act(void);

/* For those few event loops that just need SOME input, doesn't matter what */
int WaitForAnyKey (void);

int EventIsUp (SDL_Event *event);

int EventIsDown (SDL_Event *event);

int EventIsLeft (SDL_Event *event);

int EventIsRight (SDL_Event *event);

int EventIsOk (SDL_Event *event);

int EventIsCancel (SDL_Event *event);

int EventIsStepLeft (SDL_Event *event);

int EventIsStepRight (SDL_Event *event);

int EventIsStepBack (SDL_Event *event);

int EventIsFta (SDL_Event *event);

int EventIsMisc (SDL_Event *event);

#endif /* HAVE_UILOOP_H */












