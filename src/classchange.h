/*  classchange.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_CLASSCHANGE_H
#define HAVE_CLASSCHANGE_H

typedef struct GwizClassChanger_ GwizClassChanger;

struct GwizClassChanger_ {
    SDL_Surface *displace;
    SDL_Surface *area;
    PlayerPawn *pawn;
    SDL_Rect crect;
    SDL_Rect drect;
    int position;
    int maxposition;
    int yoffset;
};

void NewClassEligibilityList (PlayerPawn *pawn);

void InitClassChanger (void);

void MoveClassChangeCursor (int goingup);

int ClassChangeLoop (void);

void CountAvailableClassesToPawn (void);

int CheckPawnEligibilityForClass (PlayerPawn *pawn, int class);

void SetPawnClass (PlayerPawn *pawn, int classno);

#endif /* HAVE_CLASSCHANGE_H */
