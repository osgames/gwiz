/*  shop.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_SHOP_H
#define HAVE_SHOP_H

typedef struct GwizShopWindow_ GwizShopWindow;

struct GwizShopWindow_ {
    PlayerPawn *pawn;      /* the pawn performing the transaction */
    GwizShopNode *gsn;     /* reference point for the functions */
    SDL_Surface *area;     /* the actual shop window */
    SDL_Surface *displace; /* The surface behind the window */
    SDL_Surface *list;     /* the actual list surface */
    SDL_Rect lrect;        /* the list coords */
    SDL_Rect prect;        /* the pricelist coords */
    SDL_Rect crect;        /* cursor rect */
    SDL_Rect disrect;      /* The coords of the surface behind the window */

    SDL_Rect rlist;        /* part of the image to read from: top */
    SDL_Rect txtrect;      /* The coordinates of the top part of the list */

    int focusposition;     /* where the list begins */
    int position;          /* Where the cursor is */
    int maxposition;       /* distance before wrapping takes effect */
};

/* Clear the last values used for the shop window */
void PrepShopWindow (int maxposition);

void Buy (int which);

void PrepSellWindow (void);

void Sell (int which);

/* could use existing functions.  FIXME: shouldn't duplicate functionality. */
void RenderItemsForSale (void);

void MoveSaleCursor (int goingup);

void SalesLoop (void);

int RequestSale (Inventory *item);

/* create a new surface showing your options */
void RenderShopListView (void);

/* Set the coordinates of the rectangles to blit */
void SetShopViewCoords (void);

/* Shift the cursor to a new position */
void MoveShopCursor (int goingup);

void RefreshShopCursor (void);

void ShopListLoop (void);

void PurchaseItem (void);

#endif /* HAVE_SHOP_H */
