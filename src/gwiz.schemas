<gconfschemafile>
 <schemalist>
  <schema>
   <key>/schemas/apps/gwiz/fullscreen</key>
   <applyto>/apps/gwiz/fullscreen</applyto>
   <owner>gwiz</owner>
   <type>bool</type>
   <locale name="C">
    <default>false</default>
    <short>Enable/disable fullscreen mode</short>
    <long>Enables or disables fullscreen mode.  Fullscreen mode is NOT recommended for CVS or unstable builds.</long>
  </locale>
 </schema>
 <schema>
   <key>/schemas/apps/gwiz/hwrender</key>
   <applyto>/apps/gwiz/hwrender</applyto>
   <owner>gwiz</owner>
   <type>bool</type>
   <locale name="C">
    <default>true</default>
    <short>Request hardware rendering</short>
    <long>Enables or disables hardware rendering.  Hardware rendering is required for double buffering..</long>
  </locale>
 </schema>
 <schema>
   <key>/schemas/apps/gwiz/doublebuf</key>
   <applyto>/apps/gwiz/doublebuf</applyto>
   <owner>gwiz</owner>
   <type>bool</type>
   <locale name="C">
    <default>true</default>
    <short>Enable/disable double buffering</short>
    <long>Enables or disables double buffering for video rendering activities.  May impact performance negatively, but gets rid of "flickering" when redrawing the walls.  This setting requires hwrender, and is recommended.</long>
  </locale>
 </schema>
 <schema>
   <key>/schemas/apps/gwiz/color_depth</key>
   <applyto>/apps/gwiz/color_depth</applyto>
   <owner>gwiz</owner>
   <type>int</type>
   <locale name="C">
    <default>32</default>
    <short>Set color depth to use.</short>
    <long>Potential depths are 32 (recommended), 24 (sometimes acceptable), 16 or 15 (permissible on slow machines), or 8 (not recommended).  Decreasing color depth may help performance, but will make the game look worse.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/ptsize</key>
  <applyto>/apps/gwiz/ptsize</applyto>
  <owner>gwiz</owner>
  <type>int</type>
  <locale name="C">
   <default>16</default>
   <short>Font size</short>
   <long>Size of font to use in gwiz (1-24).  This setting may cause unintended behavior (text rolling off the edge of the screen, etc)</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/logging</key>
  <applyto>/apps/gwiz/logging</applyto>
  <owner>gwiz</owner>
  <type>bool</type>
  <locale name="C">
   <default>true</default>
   <short>Whether to write logs</short>
   <long>Logs can provide more detailed output of what was going on at the time of a crash.  Log size should not get out of hand because every time the game is restarted, the log is expunged and started over.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/zaa</key>
  <applyto>/apps/gwiz/zaa</applyto>
  <owner>gwiz</owner>
  <type>bool</type>
  <locale name="C">
   <default>true</default>
   <short>Antialias surfaces when zoomed</short>
   <long>Zoomed Antialiasing is only available when the color depth is set to 32bpp, given the limitations of color mixing and palette availability.  Causes some slowdown when rendering walls for the first time, but after that, none at all.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/joy/enabled</key>
  <applyto>/apps/gwiz/joy/enabled</applyto>
  <owner>gwiz</owner>
  <type>bool</type>
  <locale name="C">
   <default>true</default>
   <short>Enable the joystick(s)</short>
   <long>Enable support for joysticks.  If your kernel detects no joystick, neither will SDL (and joystick support will be disabled).  If multiple joysticks are present, you will be given your choice in which device to configure.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/joy/which</key>
  <applyto>/apps/gwiz/joy/which</applyto>
  <owner>gwiz</owner>
  <type>int</type>
  <locale name="C">
   <default>-1</default>
   <short>Which joystick to use</short>
   <long>SDL registers joystick by number, not device name.  You probably should use the in-game configurator to set this value (F12 in game)</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/joy/step_left</key>
  <applyto>/apps/gwiz/joy/step_left</applyto>
  <owner>gwiz</owner>
  <type>int</type>
  <locale name="C">
   <default>-1</default>
   <short>Joybutton to step left</short>
   <long>Determines which joystick button responds by attempting to step to the left once.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/joy/step_right</key>
  <applyto>/apps/gwiz/joy/step_right</applyto>
  <owner>gwiz</owner>
  <type>int</type>
  <locale name="C">
   <default>-1</default>
   <short>Joybutton to step right</short>
   <long>Determines which joystick button responds by attempting to step to the right once.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/joy/step_back</key>
  <applyto>/apps/gwiz/joy/step_back</applyto>
  <owner>gwiz</owner>
  <type>int</type>
  <locale name="C">
   <default>-1</default>
   <short>Joybutton to step back</short>
   <long>Determines which joystick button responds by attempting to step back once.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/joy/button_ok</key>
  <applyto>/apps/gwiz/joy/button_ok</applyto>
  <owner>gwiz</owner>
  <type>int</type>
  <locale name="C">
   <default>0</default>
   <short>Button used for confirmation</short>
   <long>Determines which button will be used to accept the selected choice when presented with some menu.  Also used to "kick" (open) doors in the maze and step through.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/joy/cancel</key>
  <applyto>/apps/gwiz/joy/cancel</applyto>
  <owner>gwiz</owner>
  <type>int</type>
  <locale name="C">
   <default>-1</default>
   <short>Button used for cancellation</short>
   <long>Determines which button will be used to cancel a given choice when presented with some menu.  Used to "escape" to the previous menu or activity.  Also sets camp when used in the maze.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/font_aa</key>
  <applyto>/apps/gwiz/font_aa</applyto>
  <owner>gwiz</owner>
  <type>bool</type>
  <locale name="C">
   <default>true</default>
   <short>Font Antialiasing</short>
   <long>Enable or disable antialiasing on fonts.  Will make fonts uglier, but potentially more readable for some people.  This setting is recommended.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/kmap/forward</key>
  <applyto>/apps/gwiz/kmap/forward</applyto>
  <owner>gwiz</owner>
  <type>string</type>
  <locale name="C">
   <default>Num 8</default>
   <short>Assign a key to perform a given task</short>
   <long>Assigns a key to perfgom a given task (such as move up, down, left, right).  Editing these values via gconf is NOT RECOMMENDED.  Please use the in-game configurator to ensure legitimate values.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/kmap/backward</key>
  <applyto>/apps/gwiz/kmap/backward</applyto>
  <owner>gwiz</owner>
  <type>string</type>
  <locale name="C">
   <default>Num 5</default>
   <short>Assign a key to perform a given task</short>
   <long>Assigns a key to perfgom a given task (such as move up, down, left, right).  Editing these values via gconf is NOT RECOMMENDED.  Please use the in-game configurator to ensure legitimate values.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/kmap/turn_left</key>
  <applyto>/apps/gwiz/kmap/turn_left</applyto>
  <owner>gwiz</owner>
  <type>string</type>
  <locale name="C">
   <default>Num 7</default>
   <short>Assign a key to perform a given task</short>
   <long>Assigns a key to perfgom a given task (such as move up, down, left, right).  Editing these values via gconf is NOT RECOMMENDED.  Please use the in-game configurator to ensure legitimate values.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/kmap/turn_right</key>
  <applyto>/apps/gwiz/kmap/turn_right</applyto>
  <owner>gwiz</owner>
  <type>string</type>
  <locale name="C">
   <default>Num 9</default>
   <short>Assign a key to perform a given task</short>
   <long>Assigns a key to perfgom a given task (such as move up, down, left, right).  Editing these values via gconf is NOT RECOMMENDED.  Please use the in-game configurator to ensure legitimate values.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/kmap/step_left</key>
  <applyto>/apps/gwiz/kmap/step_left</applyto>
  <owner>gwiz</owner>
  <type>string</type>
  <locale name="C">
   <default>Num 4</default>
   <short>Assign a key to perform a given task</short>
   <long>Assigns a key to perfgom a given task (such as move up, down, left, right).  Editing these values via gconf is NOT RECOMMENDED.  Please use the in-game configurator to ensure legitimate values.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/kmap/step_right</key>
  <applyto>/apps/gwiz/kmap/step_right</applyto>
  <owner>gwiz</owner>
  <type>string</type>
  <locale name="C">
   <default>Num 6</default>
   <short>Assign a key to perform a given task</short>
   <long>Assigns a key to perfgom a given task (such as move up, down, left, right).  Editing these values via gconf is NOT RECOMMENDED.  Please use the in-game configurator to ensure legitimate values.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/kmap/turn_around</key>
  <applyto>/apps/gwiz/kmap/turn_around</applyto>
  <owner>gwiz</owner>
  <type>string</type>
  <locale name="C">
   <default>Num 2</default>
   <short>Assign a key to perform a given task</short>
   <long>Assigns a key to perfgom a given task (such as move up, down, left, right).  Editing these values via gconf is NOT RECOMMENDED.  Please use the in-game configurator to ensure legitimate values.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/kmap/button_ok</key>
  <applyto>/apps/gwiz/kmap/button_ok</applyto>
  <owner>gwiz</owner>
  <type>string</type>
  <locale name="C">
   <default>Num Return</default>
   <short>Assign a key to perform a given task</short>
   <long>Assigns a key to perfgom a given task (such as move up, down, left, right).  Editing these values via gconf is NOT RECOMMENDED.  Please use the in-game configurator to ensure legitimate values.</long>
  </locale>
 </schema>
 <schema>
  <key>/schemas/apps/gwiz/kmap/button_cancel</key>
  <applyto>/apps/gwiz/kmap/button_cancel</applyto>
  <owner>gwiz</owner>
  <type>string</type>
  <locale name="C">
   <default>Num 0</default>
   <short>Assign a key to perform a given task</short>
   <long>Assigns a key to perfgom a given task (such as move up, down, left, right).  Editing these values via gconf is NOT RECOMMENDED.  Please use the in-game configurator to ensure legitimate values.</long>
  </locale>
 </schema>
 </schemalist>
</gconfschemafile>
