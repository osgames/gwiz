/*  video.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "gwiz.h"
#include "text.h"
#include "uiloop.h"
#include "playerpawn.h"
#include "video.h"

extern GwizApp gwiz;

void MapWalls(void)
{
    SDL_Rect frect;
    frect.x = 0;
    frect.y = gwiz.canvas->h - gwiz.floor[0]->h;
    frect.w = gwiz.canvas->w;
    frect.h = gwiz.floor[0]->h;

    SDL_FillRect (gwiz.canvas, NULL, 0);

    SDL_BlitSurface (gwiz.floor[(gwiz.ffi ? 0 : 1)], NULL, gwiz.canvas, &frect);
    gwiz.ffi = (gwiz.ffi ? 0 : 1);

    if ((gwiz.map.tile[gwiz.x][gwiz.y] & TDARK) == TDARK)
	{
	    SDL_Surface *msg = NewTextMsg ("It\'s Dark!");
	    SDL_Rect dest;
	    dest.x = gwiz.canvas->w/2 - msg->w/2;
	    dest.y = gwiz.canvas->h/2 - msg->h/2;
	    dest.h = msg->h;
	    dest.w = msg->w;
	    SDL_BlitSurface (msg, NULL, gwiz.canvas, &dest);
	    SDL_FreeSurface (msg);
	    SDL_Flip (gwiz.canvas);
	    return;
	}
    if (gwiz.cond.lumen)
	MapLightWalls();
    /*    if (gwiz.cond.volitare)
	  gwiz.cond.volitare--;
	  if (gwiz.cond.agnoscere)
	  gwiz.cond.agnoscere--;
	  if (gwiz.cond.lumen)
	  gwiz.cond.lumen--; */

    if (CheckMapObject (WALL, OBJ_FRONT, 1, -1))
	RenderObject (WALL, 0);
    if (CheckMapObject (WALL, OBJ_FRONT, 1, 0))
	RenderObject (WALL, 1);
    if (CheckMapObject (WALL, OBJ_FRONT, 1, 1))
	RenderObject (WALL, 2);

    if (CheckMapObject (DOOR, OBJ_FRONT, 1, -1))
	RenderObject (DOOR, 0);
    if (CheckMapObject (DOOR, OBJ_FRONT, 1, 0))
	RenderObject (DOOR, 1);
    if (CheckMapObject (DOOR, OBJ_FRONT, 1, 1))
	RenderObject (DOOR, 2);

    if (CheckMapObject (WALL, OBJ_LEFT, 1, -1))
	RenderObject (WALL, 3);
    if (CheckMapObject (WALL, OBJ_LEFT, 1, 0))
	RenderObject (WALL, 4);
    if (CheckMapObject (WALL, OBJ_RIGHT, 1, 0))
	RenderObject (WALL, 5);
    if (CheckMapObject (WALL, OBJ_RIGHT, 1, 1))
	RenderObject (WALL, 6);

    if (CheckMapObject (DOOR, OBJ_LEFT, 1, -1))
	RenderObject (DOOR, 3);
    if (CheckMapObject (DOOR, OBJ_LEFT, 1, 0))
	RenderObject (DOOR, 4);
    if (CheckMapObject (DOOR, OBJ_RIGHT, 1, 0))
	RenderObject (DOOR, 5);
    if (CheckMapObject (DOOR, OBJ_RIGHT, 1, 1))
	RenderObject (DOOR, 6);

    if (CheckMapObject (WALL, OBJ_FRONT, 0, -1))
	RenderObject (WALL, 7);
    if (CheckMapObject (WALL, OBJ_FRONT, 0, 0))
	RenderObject (WALL, 8);
    if (CheckMapObject (WALL, OBJ_FRONT, 0, 1))
	RenderObject (WALL, 9);

    if (CheckMapObject (DOOR, OBJ_FRONT, 0, -1))
	RenderObject (DOOR, 7);
    if (CheckMapObject (DOOR, OBJ_FRONT, 0, 0))
	RenderObject (DOOR, 8);
    if (CheckMapObject (DOOR, OBJ_FRONT, 0, 1))
	RenderObject (DOOR, 9);

    if (CheckMapObject (WALL, OBJ_LEFT, 0, 0))
	RenderObject (WALL, 10);
    if (CheckMapObject (WALL, OBJ_RIGHT, 0, 0))
	RenderObject (WALL, 11);

    if (CheckMapObject (DOOR, OBJ_LEFT, 0, 0))
	RenderObject (DOOR, 10);
    if (CheckMapObject (DOOR, OBJ_RIGHT, 0, 0))
	RenderObject (DOOR, 11);

    SDL_Flip (gwiz.canvas);
}

void MapLightWalls (void)
{
    if (CheckMapObject (WALL, OBJ_FRONT, 3, -2))
	RenderLightObject (WALL, 0);
    if (CheckMapObject (WALL, OBJ_FRONT, 3, -1))
	RenderLightObject (WALL, 1);
    if (CheckMapObject (WALL, OBJ_FRONT, 3, 0))
	RenderLightObject (WALL, 2);
    if (CheckMapObject (WALL, OBJ_FRONT, 3, 1))
	RenderLightObject (WALL, 3);
    if (CheckMapObject (WALL, OBJ_FRONT, 3, 2))
	RenderLightObject (WALL, 4);

    if (CheckMapObject (DOOR, OBJ_FRONT, 3, -2))
	RenderLightObject (DOOR, 0);
    if (CheckMapObject (DOOR, OBJ_FRONT, 3, -1))
	RenderLightObject (DOOR, 1);
    if (CheckMapObject (DOOR, OBJ_FRONT, 3, 0))
	RenderLightObject (DOOR, 2);
    if (CheckMapObject (DOOR, OBJ_FRONT, 3, 1))
	RenderLightObject (DOOR, 3);
    if (CheckMapObject (DOOR, OBJ_FRONT, 3, 2))
	RenderLightObject (DOOR, 4);

    if (CheckMapObject (WALL, OBJ_LEFT, 3, -2))
	RenderLightObject (WALL, 5);
    if (CheckMapObject (WALL, OBJ_LEFT, 3, -1))
	RenderLightObject (WALL, 6);
    if (CheckMapObject (WALL, OBJ_LEFT, 3, 0))
	RenderLightObject (WALL, 7);

    if (CheckMapObject (DOOR, OBJ_LEFT, 3, -2))
	RenderLightObject (DOOR, 5);
    if (CheckMapObject (DOOR, OBJ_LEFT, 3, -1))
	RenderLightObject (DOOR, 6);
    if (CheckMapObject (DOOR, OBJ_LEFT, 3, 0))
	RenderLightObject (DOOR, 7);

    if (CheckMapObject (WALL, OBJ_RIGHT, 3, 0))
	RenderLightObject (WALL, 8);
    if (CheckMapObject (WALL, OBJ_RIGHT, 3, 1))
	RenderLightObject (WALL, 9);
    if (CheckMapObject (WALL, OBJ_RIGHT, 3, 2))
	RenderLightObject (WALL, 10);

    if (CheckMapObject (DOOR, OBJ_RIGHT, 3, 0))
	RenderLightObject (DOOR, 8);
    if (CheckMapObject (DOOR, OBJ_RIGHT, 3, 1))
	RenderLightObject (DOOR, 9);
    if (CheckMapObject (DOOR, OBJ_RIGHT, 3, 2))
	RenderLightObject (DOOR, 10);

    if (CheckMapObject (WALL, OBJ_FRONT, 2, -2))
	RenderLightObject (WALL, 11);
    if (CheckMapObject (WALL, OBJ_FRONT, 2, -1))
	RenderLightObject (WALL, 12);
    if (CheckMapObject (WALL, OBJ_FRONT, 2, 0))
	RenderLightObject (WALL, 13);
    if (CheckMapObject (WALL, OBJ_FRONT, 2, 1))
	RenderLightObject (WALL, 14);
    if (CheckMapObject (WALL, OBJ_FRONT, 2, 2))
	RenderLightObject (WALL, 15);

    if (CheckMapObject (DOOR, OBJ_FRONT, 2, -2))
	RenderLightObject (DOOR, 11);
    if (CheckMapObject (DOOR, OBJ_FRONT, 2, -1))
	RenderLightObject (DOOR, 12);
    if (CheckMapObject (DOOR, OBJ_FRONT, 2, 0))
	RenderLightObject (DOOR, 13);
    if (CheckMapObject (DOOR, OBJ_FRONT, 2, 1))
	RenderLightObject (DOOR, 14);
    if (CheckMapObject (DOOR, OBJ_FRONT, 2, 2))
	RenderLightObject (DOOR, 15);

    if (CheckMapObject (WALL, OBJ_LEFT, 2, -1))
	RenderLightObject (WALL, 16);
    if (CheckMapObject (WALL, OBJ_LEFT, 2, 0))
	RenderLightObject (WALL, 17);

    if (CheckMapObject (DOOR, OBJ_LEFT, 2, -1))
	RenderLightObject (DOOR, 16);
    if (CheckMapObject (DOOR, OBJ_LEFT, 2, 0))
	RenderLightObject (DOOR, 17);

    if (CheckMapObject (WALL, OBJ_RIGHT, 2, 0))
	RenderLightObject (WALL, 18);
    if (CheckMapObject (WALL, OBJ_RIGHT, 2, 1))
	RenderLightObject (WALL, 19);

    if (CheckMapObject (DOOR, OBJ_RIGHT, 2, 0))
	RenderLightObject (DOOR, 18);
    if (CheckMapObject (DOOR, OBJ_RIGHT, 2, 1))
	RenderLightObject (DOOR, 19);

    if (CheckMapObject (WALL, OBJ_FRONT, 1, -2))
	RenderLightObject (WALL, 20);
    if (CheckMapObject (WALL, OBJ_FRONT, 1, 2))
	RenderLightObject (WALL, 21);

    if (CheckMapObject (DOOR, OBJ_FRONT, 1, -2))
	RenderLightObject (DOOR, 20);
    if (CheckMapObject (DOOR, OBJ_FRONT, 1, 2))
	RenderLightObject (DOOR, 21);
}

void RenderObject (int obj, int pos)
{
    SDL_Rect dest;
    SDL_Surface **src = (obj == WALL) ? gwiz.wall : gwiz.door;
    switch (pos)
	{
	case 0:
	    dest.x = 40;
	    dest.y = 180;
	    dest.w = src[14]->w;
	    dest.h = src[14]->h;
	    SDL_BlitSurface (src[14], NULL, gwiz.canvas, &dest);
	    break;
	case 1:
	    dest.x = 280;
	    dest.y = 180;
	    dest.w = src[14]->w;
	    dest.h = src[14]->h;
	    SDL_BlitSurface (src[14], NULL, gwiz.canvas, &dest);
	    break;
	case 2:
	    dest.x = 520;
	    dest.y = 180;
	    dest.w = src[14]->w;
	    dest.h = src[14]->h;
	    SDL_BlitSurface (src[14], NULL, gwiz.canvas, &dest);
	    break;

	case 3:
	    dest.x = 40 - src[15]->w;
	    dest.y = 60;
	    dest.w = src[15]->w;
	    dest.h = src[15]->h;
	    SDL_BlitSurface (src[15], NULL, gwiz.canvas, &dest);
	    break;
	case 4:
	    dest.x = 160;
	    dest.y = 60;
	    dest.w = src[16]->w;
	    dest.h = src[16]->h;
	    SDL_BlitSurface (src[16], NULL, gwiz.canvas, &dest);
	    break;
	case 5:
	    dest.x = 520;
	    dest.y = 60;
	    dest.w = src[17]->w;
	    dest.h = src[17]->h;
	    SDL_BlitSurface (src[17], NULL, gwiz.canvas, &dest);
	    break;
	case 6:
	    dest.x = 760;
	    dest.y = 60;
	    dest.w = src[18]->w;
	    dest.h = src[18]->h;
	    SDL_BlitSurface (src[18], NULL, gwiz.canvas, &dest);
	    break;

	case 7:
	    dest.x = -320;
	    dest.y = 60;
	    dest.w = src[19]->w;
	    dest.h = src[19]->h;
	    SDL_BlitSurface (src[19], NULL, gwiz.canvas, &dest);
	    break;
	case 8:
	    dest.x = 160;
	    dest.y = 60;
	    dest.w = src[19]->w;
	    dest.h = src[19]->h;
	    SDL_BlitSurface (src[19], NULL, gwiz.canvas, &dest);
	    break;
	case 9:
	    dest.x = 640;
	    dest.y = 60;
	    dest.w = src[19]->w;
	    dest.h = src[19]->h;
	    SDL_BlitSurface (src[19], NULL, gwiz.canvas, &dest);
	    break;

	case 10:
	    dest.x = 0;
	    dest.y = 0;
	    dest.w = src[20]->w;
	    dest.h = src[20]->h;
	    SDL_BlitSurface (src[20], NULL, gwiz.canvas, &dest);
	    break;
	case 11:
	    dest.x = 640;
	    dest.y = 0;
	    dest.w = src[21]->w;
	    dest.h = src[21]->h;
	    SDL_BlitSurface (src[21], NULL, gwiz.canvas, &dest);
	    break;
	}
}

void RenderLightObject (int obj, int pos)
{
    SDL_Rect dest;
    SDL_Surface **src = (obj == WALL) ? gwiz.wall : gwiz.door;

    switch (pos)
	{
	case 0:
	    dest.x = 250;
	    dest.y = gwiz.canvas->h/2 - src[0]->h/2;
	    dest.h = gwiz.wall[0]->h;
	    dest.w = gwiz.wall[0]->w;
	    SDL_BlitSurface (src[0], NULL, gwiz.canvas, &dest);
	    break;
	case 1:
	    dest.x = 310;
	    dest.y = gwiz.canvas->h/2 - src[0]->h/2;
	    dest.h = gwiz.wall[0]->h;
	    dest.w = gwiz.wall[0]->w;
	    SDL_BlitSurface (src[0], NULL, gwiz.canvas, &dest);
	    break;
	case 2:
	    dest.x = 370;
	    dest.y = gwiz.canvas->h/2 - src[0]->h/2;
	    dest.h = gwiz.wall[0]->h;
	    dest.w = gwiz.wall[0]->w;
	    SDL_BlitSurface (src[0], NULL, gwiz.canvas, &dest);
	    break;
	case 3:
	    dest.x = 430;
	    dest.y = gwiz.canvas->h/2 - src[0]->h/2;
	    dest.h = gwiz.wall[0]->h;
	    dest.w = gwiz.wall[0]->w;
	    SDL_BlitSurface (src[0], NULL, gwiz.canvas, &dest);
	    break;
	case 4:
	    dest.x = 490;
	    dest.y = gwiz.canvas->h/2 - src[0]->h/2;
	    dest.h = gwiz.wall[0]->h;
	    dest.w = gwiz.wall[0]->w;
	    SDL_BlitSurface (src[0], NULL, gwiz.canvas, &dest);
	    break;

	case 5:
	    dest.x = 187;
	    dest.y = gwiz.canvas->h/2 - src[1]->h/2;
	    dest.h = 120;
	    dest.w = 45+45/2;
	    SDL_BlitSurface (src[1], NULL, gwiz.canvas, &dest);
	    break;
	case 6:
	    dest.x = 265;
	    dest.y = gwiz.canvas->h/2 - src[2]->h/2;
	    dest.h = 120;
	    dest.w = 45;
	    SDL_BlitSurface (src[2], NULL, gwiz.canvas, &dest);
	    break;
	case 7:
	    dest.x = 340;
	    dest.y = gwiz.canvas->h/2 - src[3]->h/2;
	    dest.h = 120;
	    dest.w = 30;
	    SDL_BlitSurface (src[3], NULL, gwiz.canvas, &dest);
	    break;

	case 8:
	    dest.x = 430;
	    dest.y = gwiz.canvas->h/2 - src[4]->h/2;
	    dest.h = 120;
	    dest.w = 30;
	    SDL_BlitSurface (src[4], NULL, gwiz.canvas, &dest);
	    break;
	case 9:
	    dest.x = 490;
	    dest.y = gwiz.canvas->h/2 - src[5]->h/2;
	    dest.h = 120;
	    dest.w = 45;
	    SDL_BlitSurface (src[5], NULL, gwiz.canvas, &dest);
	    break;
	case 10:
	    dest.x = 520;
	    dest.y = gwiz.canvas->h/2 - src[6]->h/2;
	    dest.h = 120;
	    dest.w = 45+45/2;
	    SDL_BlitSurface (src[6], NULL, gwiz.canvas, &dest);
	    break;

	case 11:
	    dest.x = 100;
	    dest.y = CenterVert (gwiz.canvas, src[7]);
	    dest.h = src[7]->h;
	    dest.w = src[7]->w;
	    SDL_BlitSurface (src[7], NULL, gwiz.canvas, &dest);
	    break;
	case 12:
	    dest.x = 220;
	    dest.y = CenterVert (gwiz.canvas, src[7]);
	    dest.h = src[7]->h;
	    dest.w = src[7]->w;
	    SDL_BlitSurface (src[7], NULL, gwiz.canvas, &dest);
	    break;
	case 13:
	    dest.x = 340;
	    dest.y = CenterVert (gwiz.canvas, src[7]);
	    dest.h = src[7]->h;
	    dest.w = src[7]->w;
	    SDL_BlitSurface (src[7], NULL, gwiz.canvas, &dest);
	    break;

	case 14:
	    dest.x = 460;
	    dest.y = CenterVert (gwiz.canvas, src[7]);
	    dest.h = src[7]->h;
	    dest.w = src[7]->w;
	    SDL_BlitSurface (src[7], NULL, gwiz.canvas, &dest);
	    break;
	case 15:
	    dest.x = 580;
	    dest.y = CenterVert (gwiz.canvas, src[7]);
	    dest.h = src[7]->h;
	    dest.w = src[7]->w;
	    SDL_BlitSurface (src[7], NULL, gwiz.canvas, &dest);
	    break;

	    /*	case 16:
	    dest.x = gwiz.canvas->w/2 - src[9]->w/2 - src[9]->w - src[10]->w;
	    dest.y = gwiz.canvas->h/2 - src[10]->h/2;
	    dest.h = src[10]->h;
	    dest.w = src[10]->w;
	    SDL_BlitSurface (src[10], NULL, gwiz.canvas, &dest);
	    break;
	case 17:
	    dest.x = gwiz.canvas->w/2 - src[9]->w/2 - src[10]->w;
	    dest.y = gwiz.canvas->h/2 - src[10]->h/2;
	    dest.h = src[10]->h;
	    dest.w = src[10]->w;
	    SDL_BlitSurface (src[10], NULL, gwiz.canvas, &dest);
	    break;

	case 18:
	    dest.x = gwiz.canvas->w/2 + src[9]->w/2;
	    dest.y = gwiz.canvas->h/2 - src[11]->h/2;
	    dest.h = src[11]->h;
	    dest.w = src[11]->w;
	    SDL_BlitSurface (src[11], NULL, gwiz.canvas, &dest);
	    break;
	case 19:
	    dest.x += src[6]->w*4 + src[13]->w;
	    dest.y = gwiz.canvas->h/2 - src[15]->h/2;
	    dest.h = src[11]->h;
	    dest.w = src[11]->w;
	    SDL_BlitSurface (src[11], NULL, gwiz.canvas, &dest);
	    break;

	case 20:
	    dest.x = gwiz.canvas->w/2 - src[0]->w/2 - src[0]->w*2;
	    dest.y = gwiz.canvas->h/2 - src[0]->h/2;
	    dest.h = src[0]->h;
	    dest.w = src[0]->w;
	    SDL_BlitSurface (src[0], NULL, gwiz.canvas, &dest);
	    break;
	case 21:
	    dest.x = gwiz.canvas->w/2 - src[0]->w/2 + src[0]->w*2;
	    dest.y = gwiz.canvas->h/2 - src[0]->h/2;
	    dest.h = src[0]->h;
	    dest.w = src[0]->w;
	    SDL_BlitSurface (src[0], NULL, gwiz.canvas, &dest);
	    break; */
	}
}
