/*  camp.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <stdlib.h>
#include "gwiz.h"
#include "uiloop.h"
#include "text.h"
#include "maploader.h"
#include "menus.h"
#include "camp.h"
#include "inspect.h"
#include "party.h"

extern GwizApp gwiz;

void Camp (void)
{
    char *menulisting[] = {
	"Inspect",
	"Save (D)",
	"Reorder",
	"Memo (D)",
	"Leave",
	"Quit Game",
	NULL
    };
    int menuselection = 0;
    
    while ((menuselection = NewGwizMenu (gwiz.canvas, menulisting, -1, 
					 -1, menuselection)) != -1)
        {
	    switch (menuselection)
		{
		case 0:
		    InspectPawn(0);
		    break;
		case 1:
		    ;
		    break;
		case 2:
		    ReorderParty();
		    break;
		case 3:
		    ;
		    break;
		case 4:
		    return;
		case 5:
		    VerifyQuit();
		    return;
		}
	}
}


























